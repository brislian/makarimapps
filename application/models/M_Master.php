<?php 
 
class M_Master extends CI_Model{
	 public function __construct()
	 {
	  	parent::__construct();
	    $this->db = $this->load->database('default', TRUE);
	 }

	function listtahunkurva($table){
		return $this->db->query("SELECT distinct(TAHUNKURVA) as TAHUNKURVA FROM $table where status=1");
	}
 
	function detailtahunkurva($data){
		return $this->db->query("SELECT * FROM MS_TARGETKURVA where TAHUNKURVA=$data");
	}

	function updatetahunkurva($targetkom,$varpost,$i,$tahunkurva){
		return $this->db->query("update MS_TARGETKURVA set TARGET=$varpost, TARGETKOMULATIF = '$targetkom' where TAHUNKURVA=$tahunkurva and BULANKE='$i'");
	}


	function editdetailtahunkurva($data){
		return $this->db->query("SELECT * FROM MS_TARGETKURVA where TAHUNKURVA=$data")->result_array();
	}

	function getlast($table,$id,$param){
		return $this->db->query("SELECT * FROM $table where $id = '$param' order by NO desc limit 1");
	}

	function query2join($table1,$table2,$kolom1,$kolom2,$param,$value){
		return $this->db->query("SELECT 
			a.*,b.* from $table1 a left join
			$table2 b on $kolom1 = $kolom2 where $param = $value
			");
	}
	// function insert($data,$table){
	// 	$this->db->insert($table,$data);
	// }
	function hapus($table,$kolom,$param){
		$this->db->query("DELETE FROM $table where $kolom='$param'");
	}



}