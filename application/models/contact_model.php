<?php 
/**
 * 
 */
class Contact_model extends CI_model
{
	private $table = "contact";
	private $primary = "id_contact";

	function update($data,$id){
		$this->db->where($this->primary,$id);
		$this->db->update($this->table,$data);
	}

	function delete($id){
		$this->db->where($this->primary,$id);
		$this->db->delete($this->table,$id);
	}
}
 ?>