<?php 
/**
 * 
 */
class Letter_model extends CI_model
{
	private $table = "letter_registration";
	private $primary = "id_lett";

	function update($data,$id_lett){
		$this->db->where($this->primary,$id_lett);
		$this->db->update($this->table,$data);
	}

	function delete($id_lett){
		$this->db->where($this->primary,$id_lett);
		$this->db->delete($this->table,$id_lett);
	}
}
 ?>