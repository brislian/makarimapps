<?php 
 
class M_Global extends CI_Model{
	 public function __construct()
	 {
	  	parent::__construct();
	    $this->db = $this->load->database('default', TRUE);
	 }

	function query($query){
		return $this->db->query($query);
	}

	function view($table){
		return $this->db->query("SELECT * FROM $table where status=1");
	}
 
	function insert($data,$table){
		$insert = $this->db->insert($table,$data);
		if(!$insert){
            $error = $this->db->error();
        } else {
        	$error = "success";
        }
		return $error;
	}

	function update_data($where,$data,$table){
		$update = $this->db->where($where);
		$update = $this->db->update($table,$data);
		if(!$update){
            $error = $this->db->error();
        } else {
        	$error = "success";
        }
        return $error;
	}	

	function update($table,$param){
		$this->db->query("UPDATE $table set $param");
		
	}

	function hapus($table,$param){
		$insert = $this->db->query("DELETE FROM $table where $param");
		if(!$insert){
            $error = $this->db->error();
        } else {
        	$error = "success";
        }
		return $error;
	}

	function getmultiparam($table,$param){
		// echo $this->db->last_query();
		return $this->db->query("SELECT * FROM $table where $param");
		
	}

	function selectid($col,$table,$param){
		return $this->db->query("SELECT $col FROM $table where $param order by $col desc limit 1");
		
	}

	function globalquery($param){
		// echo $this->db->last_query();
		return $this->db->query("$param");
		
	}

	function getmultiparamrows($table,$param){
		return $this->db->query("SELECT * FROM $table where $param")->num_rows();
		
	}


	function getmultiparam2($table,$param){
		return $this->db->query("SELECT * FROM $table where $param")->result_array();
		
	}

	function getmultiparam3($table,$param){
		return $this->db->query("SELECT $table where $param");
		
	}

	function getlast($table,$id,$param){
		return $this->db->query("SELECT * FROM $table where $id = '$param' order by NO desc limit 1");
	}

	function q2join($table1,$table2,$kolom1,$kolom2,$param){
		return $this->db->query("SELECT 
			a.*,b.* from $table1 a left join
			$table2 b on $kolom1 = $kolom2 where $param 
			");
	}

	function qrealisasi($table1,$table2,$kolom1,$kolom2,$param){
		return $this->db->query("SELECT 
			a.*,b.REALISASI from $table1 a left join
			$table2 b on $kolom1 = $kolom2 where $param 
			");
	}


	function query2join($table1,$table2,$kolom1,$kolom2,$param,$value){
		return $this->db->query("SELECT 
			a.*,b.* from $table1 a left join
			$table2 b on $kolom1 = $kolom2 where $param = $value
			");
	}

	function query2joinmulti($table1,$table2,$kolom1,$kolom2,$param){
		return $this->db->query("SELECT 
			a.*,b.* from $table1 a left join
			$table2 b on $kolom1 = $kolom2 where $param
			");
	}

	function query2joinmulti2($selectcol,$table1,$table2,$kolom1,$kolom2,$param){
		return $this->db->query("SELECT 
			$selectcol from $table1 a left join
			$table2 b on $kolom1 = $kolom2 where $param
			");
	}

	function getnumrowsupload($table, $kolom, $param, $param2, $param3) {
		$q = $this->db->query("SELECT * FROM $table where $kolom = '$param' and TAHUNBELANJA = '$param2' and KODEDANABELANJA='$param3'");
		return $q->num_rows();
	}

	function getnumrowsupload2($table, $kolom, $param, $param2, $param3, $param4) {
		$q = $this->db->query("SELECT * FROM $table where $kolom = '$param' and TAHUNBELANJA = '$param2' and KODEDANABELANJA='$param3' $param4");
		return $q->num_rows();
	}

	function getnumrowsupload3($table, $param) {
		$q = $this->db->query("SELECT * FROM $table where $param");
		return $q->num_rows();
	}

	function getnumrows($table, $kolom, $param) {
		$q = $this->db->query("SELECT * FROM $table where $kolom = '$param'");
		return $q->num_rows();
	}

	function getsum($table, $kolom, $param) {
		return $this->db->query("SELECT SUM($kolom) as JUMLAH from $table where $param");
	}

	function querydinas(){
		return $this->db->query("select a.*, b.NAMADINAS, b.KODEDANABELANJA, c.NOKODEURUSAN from MS_PROGRAM a left join MS_DINAS b on a.NOKODEDINAS = b.NO left join MS_SUBURUSAN c on b.NOKODESUBURUSAN = c.KODESUBURUSAN where a.STATUS='1' and a.KODEDANABELANJA = '2'");
	}

	function distinct($col,$table,$param){
		return $this->db->query("select distinct($col) as $col from $table where $param");
	}

	function countdistinct($col,$table,$param){
		return $this->db->query("select count(distinct($col)) as $col from $table where $param");
	}

	function reportprofitloss($datefrom,$dateto,$param){
		return $this->db->query("select * from (
select u2.coa_category_name,u1.coa_id,u1.coa_category_idcoa_category_no,u1.coa_name,ifnull(v1.amountcredit,0) as amountcredit,ifnull(v2.amountdebit,0) as amountdebits,ifnull(v1.amountcredit,0)-ifnull(v2.amountdebit,0) as jumlah1 from coa u1 
		left join 
			(
			select a.coa_idcoa_no,ifnull((a.jurnal_detail_amount),0) as amountcredit ,jurnal_detail_type, b.jurnal_date from jurnal_detail a
			left join jurnal b on a.jurnal_idjurnal = b.jurnal_idjurnal
			left join coa c on a.coa_idcoa_no = c.idcoa_no 
			where a.jurnal_detail_type = 'C' and b.jurnal_date between '$datefrom' and '$dateto' 
			group by idcoa_no,jurnal_detail_type
			) v1 on u1.idcoa_no = v1.coa_idcoa_no 
			left join (
			select a.coa_idcoa_no,ifnull((a.jurnal_detail_amount),0) as amountdebit,jurnal_detail_type from jurnal_detail a
			left join jurnal b on a.jurnal_idjurnal = b.jurnal_idjurnal
			left join coa c on a.coa_idcoa_no = c.idcoa_no 
			where a.jurnal_detail_type = 'D' and b.jurnal_date between '$datefrom' and '$dateto' 
			group by idcoa_no,jurnal_detail_type
			) v2 on u1.idcoa_no = v2.coa_idcoa_no 
		left join coa_category u2 on u1.coa_category_idcoa_category_no = u2.idcoa_category_no
union		
select coa_category_name,'a',idcoa_category_no,'','','','' from coa_category u3
) as t1 where coa_category_idcoa_category_no = '$param' order by coa_id desc
		");
	}

}