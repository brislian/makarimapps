<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Api extends MY_Controller {
	function __construct(){
		parent::__construct();
	
		if($this->session->userdata('status') !== "melebbu"){
			redirect(base_url("login"));
		}
	}
	
	public function getcoa(){
		$this->load->model('M_Global');
		$sql = $this->M_Global->getmultiparam("coa","coa_status = 1 and coa_type=1");
		foreach($sql->result() as $row){
		    //$rows[]=$row;
		    $data[] = array('id' => $row->idcoa_no, 'text' => $row->coa_name);

		}
		echo json_encode($data);

	}
}