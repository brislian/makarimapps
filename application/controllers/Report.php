<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Report extends MY_Controller {
	function __construct(){
		parent::__construct();
	
		if($this->session->userdata('status') !== "melebbu"){
			redirect(base_url("login"));
		}
	}
	
	public function trialbalance(){
		$this->load->model('M_Global');
		$datefrom = $this->input->post('datefrom');
		$dateto = $this->input->post('dateto');
		
		
		$data['data1'] = $this->M_Global->globalquery("select u1.coa_id,u1.coa_name,v3.amountcreditawal, v4.amountdebitawal, v1.amountcredit,v2.amountdebit from coa u1 left join 
		(
		select a.coa_idcoa_no,(a.jurnal_detail_amount) as amountcredit ,jurnal_detail_type, b.jurnal_date from jurnal_detail a
		left join jurnal b on a.jurnal_idjurnal = b.jurnal_idjurnal
		left join coa c on a.coa_idcoa_no = c.idcoa_no 
		where a.jurnal_detail_type = 'C' and b.jurnal_date between '$datefrom' and '$dateto' 
		group by idcoa_no,jurnal_detail_type
		) v1 on u1.idcoa_no = v1.coa_idcoa_no 
		left join (
		select a.coa_idcoa_no,(a.jurnal_detail_amount) as amountdebit,jurnal_detail_type from jurnal_detail a
		left join jurnal b on a.jurnal_idjurnal = b.jurnal_idjurnal
		left join coa c on a.coa_idcoa_no = c.idcoa_no 
		where a.jurnal_detail_type = 'D' and b.jurnal_date between '$datefrom' and '$dateto' 
		group by idcoa_no,jurnal_detail_type
		) v2 on u1.idcoa_no = v2.coa_idcoa_no 
		left join 
		(
		select a.coa_idcoa_no,sum(a.jurnal_detail_amount) as amountcreditawal ,jurnal_detail_type as jurnal_detailawal, b.jurnal_date from jurnal_detail a
		left join jurnal b on a.jurnal_idjurnal = b.jurnal_idjurnal
		left join coa c on a.coa_idcoa_no = c.idcoa_no 
		where a.jurnal_detail_type = 'C' and b.jurnal_date between '0000-00-00' and '$datefrom' 
		group by idcoa_no,jurnal_detail_type
		) v3 on u1.idcoa_no = v3.coa_idcoa_no 
		left join (
		select a.coa_idcoa_no,sum(a.jurnal_detail_amount) as amountdebitawal,jurnal_detail_type as jurnal_detail_typeawal from jurnal_detail a
		left join jurnal b on a.jurnal_idjurnal = b.jurnal_idjurnal
		left join coa c on a.coa_idcoa_no = c.idcoa_no 
		where a.jurnal_detail_type = 'D' and b.jurnal_date between '0000-00-00' and '$datefrom'
		group by idcoa_no,jurnal_detail_type
		) v4 on u1.idcoa_no = v4.coa_idcoa_no 
		
");
		// print_r($data1);
	    $this->render_page('report/trialbalance',$data);
	}

	public function profitloss(){
		$this->load->model('M_Global');
		$datefrom = $this->input->post('datefrom');
		$dateto = $this->input->post('dateto');

		$data['data1'] = $this->M_Global->reportprofitloss("$datefrom","$dateto","13");
		$data['data2'] = $this->M_Global->reportprofitloss("$datefrom","$dateto","15");
		$data['data3'] = $this->M_Global->reportprofitloss("$datefrom","$dateto","16");
		$data['data4'] = $this->M_Global->reportprofitloss("$datefrom","$dateto","17");
		$data['data5'] = $this->M_Global->reportprofitloss("$datefrom","$dateto","14");

		$this->render_page('report/profitloss',$data);

	}

	public function neraca(){
		$this->load->model('M_Global');
		$datefrom = $this->input->post('datefrom');
		$dateto = $this->input->post('dateto');

		$data['data1'] = $this->M_Global->reportprofitloss("$datefrom","$dateto","13");
		$data['data2'] = $this->M_Global->reportprofitloss("$datefrom","$dateto","15");
		$data['data3'] = $this->M_Global->reportprofitloss("$datefrom","$dateto","16");
		$data['data4'] = $this->M_Global->reportprofitloss("$datefrom","$dateto","17");
		$data['data5'] = $this->M_Global->reportprofitloss("$datefrom","$dateto","14");

		$this->render_page('report/profitloss',$data);

	}

}