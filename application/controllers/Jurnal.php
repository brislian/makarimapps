<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Jurnal extends MY_Controller {
	function __construct(){
		parent::__construct();
	
		if($this->session->userdata('status') !== "melebbu"){
			redirect(base_url("login"));
		}
	}
	
	public function index(){
		$this->load->model('M_Global');
		$data['data1'] = $this->M_Global->globalquery("select a.*, b.* from jurnal a left join jurnal_detail b on a.jurnal_idjurnal=b.jurnal_idjurnal");
	    $this->render_page('jurnal/index',$data);
	}

	public function add(){
		$this->load->model('M_Global');
		$data['data1'] = $this->M_Global->getmultiparam("coa_category","coa_category_status = 1");
		$data['data2'] = $this->M_Global->getmultiparam("coa","coa_status = 1");
	    $this->render_page('jurnal/add',$data);
	}

	public function edit(){
		$this->load->model('M_Global');
		$idjurnal = $this->input->get(id);
		$idjurnal = $this->input->get(id);
		$data['data1'] = $this->M_Global->getmultiparam("coa_category","coa_category_status = 1");
		$data['data2'] = $this->M_Global->getmultiparam("coa","coa_status = 1 and coa_type=1");
	    $this->render_page('jurnal/edit',$data);
	}

	public function insert(){
		$this->load->model('M_Global');
		$accountname = $this->input->post('accountname');
		$accountdate = $this->input->post('accountdate');
		$iterasi = $this->input->post('iterasi');
		
		for($i=0;$i<=$iterasi;$i++){
		 	$accountcat[$i] = $this->input->post("accountcat".$i);
		 	$accountamount[$i] = $this->input->post("accountamount".$i);
		 	$accountdesc[$i] = $this->input->post("accountdesc".$i);
		 	$accounttype[$i] = $this->input->post("accounttype".$i);
		}

		$jurnalunique = date('ymdhms');
		$jurnalid = "JRN".$jurnalunique;

		$data1 = array(
				'jurnal_idjurnal' => $jurnalid,
				'jurnal_name' => $accountname,
				'jurnal_date' => $accountdate,
				'jurnal_status' => 1
				);
		$abc = $this->M_Global->insert($data1, "jurnal");
		print_r($abc);

		for($i=0;$i<=$iterasi;$i++){
			$data2 = array(
					'jurnal_idjurnal' => $jurnalid,
					'coa_idcoa_no' => $accountcat[$i],
					'jurnal_detail_amount' => $accountamount[$i],
					'jurnal_detail_type' => $accounttype[$i]
					);
			$abc = $this->M_Global->insert($data2, "jurnal_detail");
		}

		if($abc == "success"){
			redirect(base_url("jurnal?msg=1"));
		}
	}

	public function checkcoa(){
		$this->load->model('M_Global');
		$accountnum = $this->input->post('coa_id');
		$result = $this->M_Global->getmultiparamrows("coa","coa_id = '$accountnum'");
		echo json_encode($result);
	}

}