<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Letter extends MY_Controller {
	function __construct(){
		parent::__construct();
	
		if($this->session->userdata('status') !== "melebbu"){
			redirect(base_url("login"));
		}
		$this->load->model('letter_model','letter_registration');
	}
		public function index(){
			$this->load->model('M_Global');
		
		$data['data1'] = $this->M_Global->globalquery("select * from letter_registration order by id_lett asc");

	    $this->render_page('letter_registration/index',$data);
	}

	public function add(){
		$this->load->model('M_Global');
		
	    $this->render_page('letter_registration/add');
	}

	public function insert()
	{

		$this->load->model('M_Global');

		$date = $this->input->post('date');
		$letterno = $this->input->post('letterno');
		$clientno = $this->input->post('clientno');
		$month = $this->input->post('month');
		$year = $this->input->post('year');
		$partner = $this->input->post('partner');
		$staff1 = $this->input->post('staff1');
		$staff2 = $this->input->post('staff2');
		$sec = $this->input->post('sec');
		$tocomp = $this->input->post('tocomp');
		$address = $this->input->post('address');
		$att = $this->input->post('att');
		$subject = $this->input->post('subject');
		$data1 = array(
				'date' => $date,
				'letter_no' => $letterno,
				'client_no' => $clientno,
				'month' => $month,
				'year' => $year,
				'partner' => $partner,
				'staff1' => $staff1,
				'staff2' => $staff2,
				'secretary' => $sec,
				'comp_to' => $tocomp,
				'address' => $address,
				'attention' => $att,
				'subject' => $subject
				);

		$abc = $this->M_Global->insert($data1, "letter_registration");
		print_r($abc);
		redirect('letter');
	}

	public function edit($id_lett){
		$post = $this->input->post();
		if (count($post) > 0) {

			$data1 = array(
				'date' => $date,
				'letter_no' => $letterno,
				'client_no' => $clientno,
				'month' => $month,
				'year' => $year,
				'partner' => $partner,
				'staff1' => $staff1,
				'staff2' => $staff2,
				'secretary' => $sec,
				'comp_to' => $tocomp,
				'address' => $address,
				'attention' => $att,
				'subject' => $subject
				);
			$this->letter_registration->update($data1,$id_lett);

		redirect('letter');
		}
		$this->load->model('M_Global');
		$data['data3'] = $this->M_Global->globalquery("select * from letter_registration where id_lett='".$id_lett."'");
	    $this->render_page('letter_registration/edit', $data);

	}

	public function delete($id_lett){
		
		$this->letter_registration->delete($id_lett);
		$this->load->model('M_Global');
		$data = $this->M_Global->globalquery("delete from letter_registration where id_lett='".$id_lett."'");
		redirect('letter');
		
	}
	
}