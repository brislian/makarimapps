<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Main extends MY_Controller {
	function __construct(){
		parent::__construct();
	
		if($this->session->userdata('status') !== "melebbu"){
			redirect(base_url("login"));
		}
	}
	
	public function index(){
		if($this->session->userdata('tipe') == "1"){
	    	$this->render_page('main');
		} else {
			$this->render_pagenonadmin('main');
		}
	}
}