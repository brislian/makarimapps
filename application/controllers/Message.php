<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Message extends Core_Controller
{

    function __construct()
    {
        parent::__construct();      
        
        $this->checkAuth();
        $this->load->model("UserModel");
        $this->load->model("MessageModel");
    }

    public function index()
        {   
            if ($this->session->userdata("group_kode_tipeakun") == 'Administrator') {
                        
            $menu_id = "7-1";
            $menu_name = "Tampilkan Pesan";
            $member_login = $this->session->userdata('fullname');
                     
            $detsQuery = $this->db->query("select * from dmessage");
                        
            $details = $detsQuery->result();

            $message_alert = "Message has been Sent.";
            $this->session->set_userdata("message_alert", $message_alert);

            $cnt = array(
            'menu_id' => $menu_id, 
            'menu_name' => $menu_name,
            'fullname' => $member_login,
            'tbpesan' => $details,
            'message_alert' => $message_alert,
            );

            $this->load_content("backend/message/index", $cnt);

             } else {
            redirect('home/error');
            }

        }

         public function tulis_pesan()
    {
        $menu_id = "7-1";
        $menu_name = "Tambah Pesan";
        $member_login = $this->session->userdata('fullname');

        $orderby = array('fullname', 'ASC');
        $cbg = $this->UserModel->getByCriteria(array('idtipeakun' => 2), null, 0, 0, null, $orderby);        

        $cnt = array(
            'menu_id' => $menu_id,
            'menu_name' => $menu_name,
            'fullname' => $member_login,
            'tb_cbg' => $cbg
        );

        $this->load_content("backend/message/tulis_pesan", $cnt);
    } 

     public function save_messagedb()
    {
        $menu_id = "11";
        $menu_name = "Save Message";
        $datez = date('Y-m-d H:i:s');
                    
        $message_id = $this->input->post('message_id');
        $user_id_to = $this->input->post('user_id_to');
        $title  = $this->input->post('title');
        $message_cont  = $this->input->post('message_cont');
        $message_date  = $this->input->post('message_date');
        $message_date_from  = $this->input->post('message_date_from');
        $message_date_to = $this->input->post('message_date_to');
        
        $iduser = $this->session->userdata("iduser");
        
        $message = $this->MessageModel->getOne(array('messageid' => $message_id));
        
        $sender_id = $message->user_id_from;
        if ($iduser == $message->user_id_from) $sender_id = $message->user_id_to;

        $user_id_to = $this->input->post('user_id_to');
        
        $sender_name = "-";
        $sender = $this->UserModel->getOne(array('iduser' => $sender_id)); 
         for ($i = 0; $i < count($user_id_to); $i++)
        {                        
            $query_db = array(
                'user_id_from' => $iduser,
                'user_id_to' => $user_id_to[$i],
                'title' => $title,
                'message_cont' => $message_cont,
                'reply_message_id' => $message_id,
                'message_date' => $message_date,
                'message_date_from' => $message_date_from,
                'message_date_to' => $message_date_to,
                'message_status' => 0,
                'message_type' => 2,            
                'datez' => $datez,
                'iduser' => $iduser,
                'read_status' => 0
            );
        $ins_message_into_db = $this->MessageModel->save($query_db);

        // if (isset($cek_menu) && is_object($cek_menu) && $cek_menu != false) { }
        //     else
        //     {
        //         $ins_message_into_db = $this->MessageModel->save($query_db);
        //     }
        }

        
        $err_message = "New Title <b>$message_cont</b> has been added.";
                        
        $message_alert = "Message has been Sent. ";
        $this->session->set_userdata("message_alert", $message_alert);

        redirect('message/index/' . $message_id);
        
        $cnt = array(
          'menu_id' => $menu_id,
          'menu_name' => $menu_name,
          'full_name' => $member_login,
          'message_alert' => $message_alert,
          'err_message' => $err_message
        );

        $this->load_content("backend/message/index", $cnt);
    }

   
    
    public function notification()
    {
        $menu_id = "1";
        $menu_name = "Message - Notification";
        $member_login = $this->session->userdata('fullname');

        $teacher_id_ok = $this->session->userdata('iduser'); 
        
        $get_message = $this->db->query("SELECT * FROM dmessage WHERE DATE(NOW()) >= message_date_from AND DATE(NOW()) <= message_date_to 
            and user_id_to = '$teacher_id_ok'
            ");
        
        $total_msg = $this->db->query("SELECT * FROM dmessage WHERE DATE(NOW()) >= message_date_from AND DATE(NOW()) <= message_date_to AND read_status = 0
            and user_id_to = '$teacher_id_ok'
            ")->num_rows();
        $messages = $get_message->result(); 

        $tesquery = $this->db->query("select * from dpendaftaran where jadwal_keberangkatan");
        $det = $tesquery->result();     
        
        $cnt = array(
            'menu_id' => $menu_id, 
            'menu_name' => $menu_name,
            'fullname' => $member_login,
            'total_msg' => $total_msg,
            'messages' => $messages
        );
        
       
        $this->load->view('notification', $cnt);
    }

    public function cobaTGL()
    {
        $sql = $this->db->query("SELECT * FROM dpendaftaran");
        foreach ($sql->result() as $value) {
            if($value->jadwal_keberangkatan != '0000-00-00'){
                $sqlDate = $this->db->query("SELECT DATE_SUB( '".$value->jadwal_keberangkatan."', INTERVAL 7 DAY) AS dating");
                foreach($sqlDate->result() as $key){
                    echo $key->dating." - ".$value->jadwal_keberangkatan."<br>";
                }
            }
        }
    }

    public function load_row()
    {
        $teacher_id_ok = $this->session->userdata('iduser'); 
        echo $this->db->query("SELECT * FROM dmessage WHERE DATE(NOW()) >= message_date_from AND DATE(NOW()) <= message_date_to AND read_status = 0
            and user_id_to = '$teacher_id_ok'
         ORDER BY messageid DESC")->num_rows();
    }

    public function load_data()
    {
        $teacher_id_ok = $this->session->userdata('iduser'); 
        $get_message = $this->db->query("SELECT * FROM dmessage WHERE DATE(NOW()) >= message_date_from AND DATE(NOW()) <= message_date_to
            and user_id_to = '$teacher_id_ok'
         ORDER BY messageid DESC");
        foreach ($get_message->result() as $message) {
            ?>
                <li class="list-group" role="presentation">
                  <div data-role="container">
                    <div data-role="content">
                      <a href="<?php echo base_url().'message/bacaPesan/'.$message->messageid ?>" class="list-group-item" role="menuitem">          
                        <div class="media">
                          <div class="media-left padding-right-10">
                            <?php 
                              if($message->read_status != 1){
                            ?>
                            <i class="icon md-receipt bg-red-600 white icon-circle" aria-hidden="true"></i>
                            <?php
                              }else{
                            ?>
                            <i class="icon md-receipt bg-blue-600 white icon-circle" aria-hidden="true"></i>
                            <?php
                              }
                            ?>
                          </div>  
                          <div class="media-body">
                            <h6 class="media-heading">&nbsp;&nbsp;<?php echo ($message->title); ?></h6>
                            <time class="media-meta" datetime="2015-06-12T20:50:48+08:00">&nbsp;&nbsp;Baca Pesan</time>
                          </div>
                        </div>
                      </a>
                    </div>
                  </div>
                </li>
            <?php
        }
    }
    
     public function delm($idc = "")
    {
        $menu_id = "1";
        $menu_name = "Delete Message";
        $member_login = $this->session->userdata('full_name');

        $class = $this->MessageModel->getOne(array('messageid' => $idc));
        if (isset($class)) {
            $this->MessageModel->delete(array('messageid' => $class->messageid));

          $this->setFlash("Message successfully deleted", "success");

            redirect(url("message/index", false));
        }
    }  

     public function editpesan($idmss = '')
    {
        $menu_id = "1";
        $menu_name = "Edituser";
        $member_login = $this->session->userdata('fullname');
                
        $messageid = "";
        $user_id_from = "";
        $title = "";
        $message_cont = "";
        $message_date ="";
        $message_date_from = "";
        $message_date_to = "";


        $fields = array('*');
        $criteria = array(
          'messageid' => $idmss    
        );
        $mss_ok = $this->MessageModel->getOne($criteria, $fields);
        if ($mss_ok) {
          $messageid = $mss_ok->messageid;
          $user_id_from = $mss_ok->user_id_from;
          $title = $mss_ok->title;
          $message_cont = $mss_ok->message_cont;
          $message_date = $mss_ok->message_date;
          $message_date_from = $mss_ok->message_date_from;
          $message_date_to = $mss_ok->message_date_to;
        }
                      
        $cnt = array(
            'iconType' => 'fa fa-plus',
            'formType' => 'edit',
            'menu_id' => $menu_id,
            'menu_name' => $menu_name,
            'fullname' => $member_login,
            'messageid' => $messageid,
            'user_id_from' => $user_id_from,
            'title' =>$title,
            'message_cont' => $message_cont,
            'message_date' => $message_date,
            'message_date_from' => $message_date_from,
            'message_date_to' => $message_date_to,
             'mss_ok' => $mss_ok
        );

        $this->load_content("backend/message/edit_pesan", $cnt);
    }

     public function updatemessage()
    {
        $menu_id = "1";
        $menu_name = "Updatepost";
        $member_login = $this->session->userdata('fullname');

        $messageid  = $this->input->post('messageid');
        $user_id_from  = $this->input->post('user_id_from');
        
        $title  = $this->input->post('title');
        $message_cont = url_title($this->input->post('message_cont'));
        $message_date  = $this->input->post('message_date');
        $message_date_from  = $this->input->post('message_date_from');
        $message_date_to  = $this->input->post('message_date_to');       
               

        $cek_message = $this->db->query("SELECT messageid FROM dmessage WHERE messageid <> '$messageid' AND (title='$title')");

        $cek_message = $cek_message->num_rows();
        if ($total_message == 0)
            {

                $datez = date('Y-m-d H:i:s');
            
                $query_db = array(
                    'iduser' => $this->session->userdata('iduser'),
                    'messageid' => $messageid,
                    'user_id_from' => $user_id_from,
                    'title' => $title,
                    'message_cont' => $message_cont,
                    'message_date' => $message_date,
                    'message_date_from' => $message_date_from,
                    'message_date_to' => $message_date_to,
                    'lastupdate' => $datez
                );

                $delivery_id = $this->MessageModel->save($query_db, array('messageid' => $messageid));

                 $err_message = "<div class='alert-box success'><span>success </span> Message has been updated.</div>";  
                $this->session->set_userdata("err_message", $err_message);

                redirect('message/index');
            }
        else
        {
            $err_message = "Title is already exist, please add another title.";

            $cnt = array(
                'menu_id' => $menu_id,
                'menu_name' => $menu_name,
                'full_name' => $member_login,
                'title' => $title,
                'err_message' => $err_message
            );

            $this->load_content("backend/message/edit_pesan", $cnt);
        }
    } 

    public function bacaPesan()
    {   
        $menu_id = "1";
        $menu_name = "Message - Notification";
        $member_login = $this->session->userdata('fullname');

        $id = $this->uri->segment(3);

        $get_message = $this->db->query("SELECT * FROM dmessage WHERE messageid=".$id);
        $dataMessage = $get_message->row();

        $cnt = array(
            'menu_id' => $menu_id,
            'menu_name' => $menu_name,
            'full_name' => $member_login,
            'pesan' => $dataMessage
        );

        $this->db->where('messageid', $id);
        $this->db->update('dmessage', array('read_status' => 1));

        $this->load_content("backend/message/bacaPesan", $cnt);
    } 

     public function cekkeberangkatan (){
        $menu_id = "1";
        $menu_name = "Message - Notification";
        $member_login = $this->session->userdata('fullname');

        $cekkQuery = $this->db->query("select *, jadwal_keberangkatan - DATE(NOW()) as selisih from dpendaftaran where jadwal_keberangkatan - DATE(NOW()) <= 5 and status_lunas = '1'");
                        
            $keberangkatan = $cekkQuery->result();

            foreach ($keberangkatan as $key => $value) {

                $idpendaftaran = $value->idpendaftaran;
                $nama = $value->nama;
                $jadwal_keberangkatan = $value->jadwal_keberangkatan;
                $status_lunas = $value->status_lunas;
                $selisih = $value->selisih;
            
              $title = "Pemberitahuan Keberangkatan";  
              $message_cont = "Jadwal Keberangkatan".$jadwal_keberangkatan.$nama;
              $datez = date('Y-m-d H:i:s');

                if ($selisih <= 5) {

                    $query_db = array(
                        'iduser' => $this->session->userdata('iduser'),
                        'user_id_from' => 1,
                        'title' => $title,
                        'message_cont' => $message_cont,
                        'message_date' => $datez,
                        'message_date_from' => $datez,
                        'message_date_to' => $datez,
                        'read_status' => 0,
                        'lastupdate' => $datez
                    );

                    $tes = $this->MessageModel->save($query_db);
                }
            }

             $cnt = array(
                        'menu_id' => $menu_id, 
                        'menu_name' => $menu_name,
                        'fullname' => $member_login,        
                    );
        
        // $this->load_content("backend/fee/detailfee", $cnt);

    }

}
