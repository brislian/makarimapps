<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Contact_program extends MY_Controller {
	function __construct(){
		parent::__construct();
	
		if($this->session->userdata('status') !== "melebbu"){
			redirect(base_url("login"));
		}
		$this->load->model('contact_model','contact');
	}
		public function index(){
			$this->load->model('M_Global');
		
		$data['data1'] = $this->M_Global->globalquery("select * from contact order by id_contact asc");

	    $this->render_page('contact_program/index',$data);
	}

	public function add(){
		$this->load->model('M_Global');
		
	    $this->render_page('contact_program/add');
	}

	public function insert()
	{

		$this->load->model('M_Global');
		$company = $this->input->post('company');
		$nama = $this->input->post('nama');
		$jobtitle = $this->input->post('jobtitle');
		$notlp = $this->input->post('notlp');
		$nofax = $this->input->post('nofax');
		$address = $this->input->post('address');
		$email = $this->input->post('email');
		$web = $this->input->post('web');


		$data1 = array(
				'company' => $company,
				'name' => $nama,
				'job_title' => $jobtitle,
				'no_tlp' => $notlp,
				'no_fax' => $nofax,
				'address' => $address,
				'email' => $email,
				'web' => $web
				);

		$abc = $this->M_Global->insert($data1, "contact");
		print_r($abc);
		redirect('contact_program');
	}

	public function edit($id){
		$post = $this->input->post();
		if (count($post) > 0) {

			$data1 = array(
				'company' => $post['company'],
				'name' => $post['nama'],
				'job_title' => $post['jobtitle'],
				'no_tlp' => $post['notlp'],
				'no_fax' => $post['nofax'],
				'address' => $post['address'],
				'email' => $post['email'],
				'web' => $post['web']
				);
			$this->contact->update($data1,$id);

		redirect('contact_program');
		}
		$this->load->model('M_Global');
		$data['data3'] = $this->M_Global->globalquery("select * from contact where id_contact='".$id."' ");
	    $this->render_page('contact_program/edit', $data);

	}

	public function delete($id){
		
		$this->contact->delete($id);
		$this->load->model('M_Global');
		$data = $this->M_Global->globalquery("delete from contact where id_contact='".$id."'");
		redirect('contact_program');
		
	}
	
}