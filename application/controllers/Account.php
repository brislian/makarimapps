<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Account extends MY_Controller {
	function __construct(){
		parent::__construct();
	
		if($this->session->userdata('status') !== "melebbu"){
			redirect(base_url("login"));
		}
	}
	
	public function index(){
		$this->load->model('M_Global');
		$data['data1'] = $this->M_Global->globalquery("select a.*, b.* from coa a left join coa_category b on a.coa_category_idcoa_category_no = b.idcoa_category_no where a.coa_status=1 order by a.coa_id asc");
	    $this->render_page('account/index',$data);
	}

	public function add(){
		$this->load->model('M_Global');
		$data['data1'] = $this->M_Global->getmultiparam("coa_category","coa_category_status = 1");
		$data['data2'] = $this->M_Global->getmultiparam("coa","coa_status = 1 and coa_type=1");
	    $this->render_page('account/add',$data);
	}

	public function edit(){
		$this->load->model('M_Global');
		$idcoa = $this->globalfunction->base64_decrypt($this->input->get(id));
		$data['data1'] = $this->M_Global->getmultiparam("coa_category","coa_category_status = 1");
		$data['data2'] = $this->M_Global->getmultiparam("coa","coa_status = 1 and coa_type=1");
		$data['data3'] = $this->M_Global->globalquery("select a.*, b.* from coa a left join coa_category b on a.coa_category_idcoa_category_no = b.idcoa_category_no where a.coa_status=1 and a.idcoa_no = '$idcoa' order by a.coa_id asc");
	    $this->render_page('account/edit',$data);
	}

	public function insert(){
		$this->load->model('M_Global');
		$accountname = $this->input->post('accountname');
		$accountnum = $this->input->post('accountnumber');
		$accountdesc = $this->input->post('accountdesc');
		$accounttype = $this->input->post('accounttype');
		$accountheader = $this->input->post('accountheader');
		$accountcategory = $this->input->post('accountcategory');
		$accountdesc = $this->input->post('accountdesc');
		$idcoano = $this->input->post('idcoano');
		$saldo = $this->input->post('saldo');
		$flag = $this->input->post('flag');
		$q1 = $this->M_Global->getmultiparam("coa_category","idcoa_category_no = '$accountcategory'");
		foreach ($q1->result() as $hasil1) {
			$activapassiva = $hasil1->coa_category_tipe;
		}

		if($activapassiva == "P") {
			$saldo = 0 - $saldo;
			$saldocredit = $saldo;
			$saldodebit = 0;
		} else {
			$saldocredit = 0;
			$saldodebit = $saldo;
		}

		if($accounttype == 1){
			$accountheader = "";
		} 

		if($flag=="edit"){
			$coaid = $accountcategory."-".$accountnum;
			$data = array(
				'coa_name' => $accountname,
				'coa_id' => $coaid,
				'coa_category_idcoa_category_no' => $accountcategory, 
				'coa_header' => $accountheader,
				'coa_type' => $accounttype,
				'coa_status' => 1,
				'coa_saldo_awal' => $saldo,
				'coa_saldo_debit' => $saldodebit,
				'coa_saldo_credit' => $saldocredit,
				'coa_desc' => $accountdesc
 				);
			$where = array(
					"idcoa_no" => $idcoano
				);

			$abc = $this->M_Global->update_data($where,$data,"coa");
			
		} else {
			$data = array(
				'coa_name' => $accountname,
				'coa_id' => $accountcategory."-".$accountnum,
				'coa_category_idcoa_category_no' => $accountcategory, 
				'coa_header' => $accountheader,
				'coa_type' => $accounttype,
				'coa_status' => 1,
				'coa_saldo_awal' => $saldo,
				'coa_saldo_debit' => $saldodebit,
				'coa_saldo_credit' => $saldocredit,
				'coa_date_create' => date('Y-m-d'),
				'coa_desc' => $accountdesc
				);
			$abc = $this->M_Global->insert($data, "coa");	

		}
		
		print_r($abc);
		if($abc == "success"){
			redirect(base_url("account?msg=1"));
		}
	}

	public function delete(){
		$this->load->model('M_Global');
		$idcoa = $this->globalfunction->base64_decrypt($this->input->get(id));
		// echo $idcoa;
		$abc = $this->M_Global->hapus("coa","idcoa_no = '$idcoa'");
		if($abc == "success"){
			redirect(base_url("account?msg=1"));
		} else {
			redirect(base_url("account?msg=2"));
		}
	}

	public function checkcoa(){
		$this->load->model('M_Global');
		$accountnum = $this->input->post('coa_id');
		$result = $this->M_Global->getmultiparamrows("coa","coa_id = '$accountnum'");
		echo json_encode($result);
	}

}