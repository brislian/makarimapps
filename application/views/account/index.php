<div class="page">
  <div class="page-header">
    <h1 class="page-title">Daftar Akun</h1>
    <div class="page-header-actions">
      <button type="button" class="btn btn-sm btn-icon btn-primary btn-round waves-effect waves-classic" data-toggle="tooltip" data-original-title="Tambah Akun" onclick="window.location.href=window.location.pathname+'/add'">
        <i class="icon md-plus" aria-hidden="true"></i>
      </button>
      <!-- <button type="button" class="btn btn-sm btn-icon btn-primary btn-round waves-effect waves-classic" data-toggle="tooltip" data-original-title="Refresh">
        <i class="icon md-refresh-alt" aria-hidden="true"></i>
      </button>
      <button type="button" class="btn btn-sm btn-icon btn-primary btn-round waves-effect waves-classic" data-toggle="tooltip" data-original-title="Setting">
        <i class="icon md-settings" aria-hidden="true"></i>
      </button> -->
    </div>
  </div>
  <div class="page-content">
        <!-- Panel -->
        <div class="panel">
          <div class="panel-body">
            <div class="example-wrap">
              <div class="example">
                <div class="table-responsive">
                  <table class="table table-hover table-striped" cellspacing="0" id="exampleTableTools">
                    <thead>
                      <tr >
                        <th>Kode Akun</th>
                        <th>Nama Akun</th>
                        <th>Kategori Akun</th>
                        <th>Saldo</th>
                        <th>Aksi</th>
                      </tr>
                    </thead>
                    <tbody>
                    <?php 
                    	foreach($data1->result() as $hasil) { 
                    		$id= $i;
                    	  $url = $this->globalfunction->base64_encrypt($hasil->id_user);
                    		?>
                      <tr>
                        <td><?php echo $hasil->coa_id; ?></td>
                        <td <?php if($hasil->coa_type == 0){ echo "style='padding-left: 20px'"; } ?>><?php echo $hasil->coa_name; ?></td>
                        <td><?php echo $hasil->coa_category_name; ?></td>
                        <td><?php echo $hasil->coa_saldo_awal; ?></td>
                        <td>
                        <button type="button" class="btn btn-icon btn-success" onclick="location.href='<?php echo base_url()."account/edit?id=".$url ?>'"><i class="icon md-edit" aria-hidden="true" "></i></button>
                        <button type="button" class="btn btn-icon btn-danger"" id="confirm" data-plugin="alertify"
                      data-type="confirm" data-confirm-title="Anda yakin Menghapus Data ini ?"
                      data-error-message="Dibatalkan" data-success-message="<?php echo base_url()."account/delete?id=".$url ?>"><i class="icon md-delete" aria-hidden="true" "></i></button>
                        </td>
                      </tr>
                     <?php } ?>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
            <!-- End Example Responsive -->
           </div>
        </div>
  </div>
</div>
<!-- End Page -->
 
  

 	


<script type="text/javascript">      
  $('#exampleTableTools').DataTable({
  "paging": true,
  "lengthChange": false,
  "searching": false,
  "ordering": false,
  "info": true,
  "autoWidth": true
});


</script>

<script>
    toastr.options = {
      "closeButton": true,
      "debug": false,
      "newestOnTop": false,
      "progressBar": false,
      "positionClass": "toast-top-right",
      "preventDuplicates": false,
      "onclick": null,
      "showDuration": "300",
      "hideDuration": "500",
      "timeOut": "5000",
      "extendedTimeOut": "1000",
      "showEasing": "swing",
      "hideEasing": "linear",
      "showMethod": "fadeIn",
      "hideMethod": "fadeOut"
    }
</script>

<?php 
    if(isset($_GET['msg'])){
        if($_GET['msg']=="delsuccess"){ 
            echo  "<script>";
            echo   "toastr['success']('Data Berhasil dihapus');";
            echo  "</script>";
        } 
    }
?>