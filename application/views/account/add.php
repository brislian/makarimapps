<div class="page">
  <div class="page-header">
    <h1 class="page-title">Tambah Akun Baru</h1>
    <div class="page-header-actions">
      <!-- <button type="button" class="btn btn-sm btn-icon btn-primary btn-round waves-effect waves-classic" data-toggle="tooltip" data-original-title="Edit">
        <i class="icon md-edit" aria-hidden="true"></i>
      </button>
      <button type="button" class="btn btn-sm btn-icon btn-primary btn-round waves-effect waves-classic" data-toggle="tooltip" data-original-title="Refresh">
        <i class="icon md-refresh-alt" aria-hidden="true"></i>
      </button>
      <button type="button" class="btn btn-sm btn-icon btn-primary btn-round waves-effect waves-classic" data-toggle="tooltip" data-original-title="Setting">
        <i class="icon md-settings" aria-hidden="true"></i>
      </button> -->
    </div>
  </div>
  <div class="page-content">
    <!-- Panel -->
    <div class="panel">
      <div class="panel-body">
        <div class="col-md-4">
          <form action="<?php base_url()?>insert" method="post" id="thisform">
          <div class="form-group">
              <h4 class="">Nama Akun</h4>
              <input type="text" class="form-control" name="accountname" autocapitalize="words">
          </div>
          <div class="form-group">
              <h4 class="">Nomor</h4>
              <div class="input-group">
              <span class="input-group-addon" id="prefix">1 - </span><input type="text" class="form-control" name="accountnumber" id="accountnumber">
              </div>
          </div>
          <div class="form-group">
              <h4 class="">Deskripsi</h4>
              <textarea class="form-control" name="accountdesc" rows="5"></textarea>
          </div>
          <div class="form-group">
              <h4 class="">Tipe Akun</h4>
              <div class="radio-custom radio-primary">
                <input type="radio" id="akunheaderyes" name="accounttype" value="1" onclick="cektipeakun()">
                <label for="inputRadiosChecked" style="padding-right: 50px">Akun Header</label>
                <input type="radio" id="akunheaderno" name="accounttype" value="0" onclick="cektipeakun()">
                <label for="inputRadiosChecked">Akun Turunan</label>
              </div>
          </div>
          <div class="form-group" id="kategoriform">
              <h4 class="">Kategori</h4>
                <select class="form-control" data-plugin="select2" name="accountcategory" id="accountcategory" onchange="cektipeakun()">
                  <?php foreach ($data1->result() as $hasil) {
                    echo "<option value=$hasil->idcoa_category_no>$hasil->coa_category_name</option>";
                  }

                  ?>
                  
                </select>
          </div>
          <div class="form-group" id="turunanform">
              <h4 class="">Akun Turunan Dari</h4>
                <select class="form-control" data-plugin="select2" name="accountheader" id="accountheader" onchange="cektipeakun()">

                  <?php foreach ($data2->result() as $hasil) {
                    echo "<option value=$hasil->idcoa_no data-satu=$hasil->coa_category_idcoa_category_no>$hasil->coa_name</option>";
                    # code...
                    //echo "<option value=".$hasil->idcoa_category_no.">".$hasil->idcoa_category_name."</select>";
                  }

                  ?>
                  
                </select>
          </div>
          <div class="form-group">
              <h4 class="">Saldo Awal</h4>
              <input type="text" class="form-control" name="saldo">
          </div>          
        </div>
        <!-- End Example Responsive -->
      </div>
    </div>
    <div class="form-group text-right">
        <button type="button" class="btn btn-warning waves-effect waves-classic" onclick="window.history.back()">Cancel</button>
        <button type="button" class="btn btn-primary waves-effect waves-classic" id="btnsubmit">Submit </button>
    </div>
    </form>
  </div>
</div>
<!-- End Page -->



<script type="text/javascript"> 
$('#turunanform').hide();

  $('#exampleTableTools').DataTable({
  "paging": true,
  "lengthChange": false,
  "searching": true,
  "ordering": true,
  "info": true,
  "autoWidth": false
});

function cektipeakun(){
  if($('#akunheaderyes').is(':checked')){
    a = "akunheader";
  } 
  if($('#akunheaderno').is(':checked')){
    a = "akunturunan";
  } 

      var acc = $('#accountnumber').val();
      if(acc == ""){
        alertify.theme('bootstrap');
        alertify.alert('Nomor Akun Belum Diisi');
        $('#akunheaderyes').prop('checked', false);
        $('#akunheaderno').prop('checked', false);

      } else {
          if (a == "akunheader") {
            accountnum =  $('#accountcategory').find(":selected").val() + "-" + $('#accountnumber').val();
            $('#turunanform').hide();
            $('#kategoriform').show();
            $('#prefix').text($('#accountcategory').find(":selected").val()+' - ');
          } else {
            accountnum =  $('#accountheader').find(":selected").data('satu') + "-" + $('#accountnumber').val();
            $('#turunanform').show();
            $('#kategoriform').hide();
            $('#prefix').text($('#accountheader').find(":selected").data('satu')+' - ');
          }
      }
}


$('#btnsubmit').click(function() {
   cektipeakun();
   if ($('#accountnumber').val() !== "") {
     $.ajax({
        url: "<?php echo base_url()?>account/checkcoa",
        type: 'POST',
        data: { 'coa_id' : accountnum },
        success: function(result){
                   if(result > 0){
                      alertify.alert('Nomor Akun Sudah Pernah Ada');
                      $('#accountnumber').val('');
                   }
                   else{
                      $('#thisform').submit();
                      //alert(result);
                       // do something if username doesn't exist
                   }
                 }
        });
    }
});
</script>