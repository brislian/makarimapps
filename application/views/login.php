    <div class="page vertical-align text-center" data-animsition-in="fade-in" data-animsition-out="fade-out" style="background-color: #0bb2d4 !important">
      <div class="page-content vertical-align-middle animation-fade animation-duration-1">
        <div class="panel">
          <div class="panel-body">
            <div class="brand">
              <img class="brand-img" src="<?php echo base_url() ?>template/assets/images/logo-colored.png" alt="...">
              <br><br>
              <h2 class="font-size-18">JurnalKu</h2>
            </div>
            <form method="post" action="<?php echo base_url();?>login/ceklogin">
              <div class="form-group form-material floating" data-plugin="formMaterial">
                <input type="text" class="form-control" name="namanya" autocomplete="off" />
                <label class="floating-label"  style="font-size: 14px">Email</label>
              </div>
              <div class="form-group form-material floating" data-plugin="formMaterial">
                <input type="password" class="form-control" name="password" />
                <label class="floating-label" style="font-size: 14px">Password</label>
              </div>
              <div class="form-group clearfix">
                
                <!-- <a class="float-right" href="forgot-password.html">Forgot password?</a> -->
              </div>
              <button type="submit" class="btn btn-primary btn-block btn-lg mt-40">Login</button>
            </form>
            <p>Still no account? Please go to <a href="register-v3.html">Sign up</a></p>
          </div>
        </div>

        <footer class="page-copyright page-copyright-inverse">
          <p></p>
          <p>© 2018. All RIGHT RESERVED.</p>
          <div class="social">
            <a class="btn btn-icon btn-pure" href="javascript:void(0)">
            <i class="icon bd-twitter" aria-hidden="true"></i>
          </a>
            <a class="btn btn-icon btn-pure" href="javascript:void(0)">
            <i class="icon bd-facebook" aria-hidden="true"></i>
          </a>
            <a class="btn btn-icon btn-pure" href="javascript:void(0)">
            <i class="icon bd-google-plus" aria-hidden="true"></i>
          </a>
          </div>
        </footer>
      </div>
    </div>
