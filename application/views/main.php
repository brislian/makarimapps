<div class="page">
  <div class="page-content container-fluid">

		<div class="panel">
          <div class="panel-body container-fluid">
          	
          	<div class="row">
          		<div class="col-md-3">
	            <!-- Card -->
	            <div class="card card-block p-30 bg-blue-600">
	              <div class="card-watermark darker font-size-80 m-15"><i class="icon md-account" aria-hidden="true"></i></div>
	              <div class="counter counter-md counter-inverse text-left">
	                <div class="counter-number-group">
	                  <span class="counter-number">1218</span>
	                  <span class="counter-number-related text-capitalize">Member</span>
	                </div>
	                <div class="counter-label text-capitalize">Total APJII Members</div>
	              </div>
	            </div>
	            <!-- End Card -->
	          </div>

	          <div class="col-md-3">
	            <!-- Card -->
	            <div class="card card-block p-30 bg-green-600">
	              <div class="card-watermark darker font-size-80 m-15"><i class="icon md-assignment-check" aria-hidden="true"></i></div>
	              <div class="counter counter-md counter-inverse text-left">
	                <div class="counter-number-group">
	                  <span class="counter-number">661</span>
	                  <span class="counter-number-related text-capitalize">Document</span>
	                </div>
	                <div class="counter-label text-capitalize">Total Dokumen</div>
	              </div>
	            </div>
	            <!-- End Card -->
	          </div>

	          <div class="col-md-3">
	            <!-- Card -->
	            <div class="card card-block p-30 bg-red-600">
	              <div class="card-watermark darker font-size-80 m-15"><i class="icon" aria-hidden="true">v4</i></div>
	              <div class="counter counter-md counter-inverse text-left">
	                <div class="counter-number-group">
	                  <span class="counter-number">451</span>
	                  <span class="counter-number-related text-capitalize">Block</span>
	                </div>
	                <div class="counter-label text-capitalize">Total IPV4</div>
	              </div>
	            </div>
	            <!-- End Card -->
	          </div>

	          <div class="col-md-3">
	            <!-- Card  -->
	            <div class="card card-block p-30 bg-purple-600">
	              <div class="card-watermark lighter font-size-80 m-15"><i class="icon" aria-hidden="true">v6</i></div>
	              <div class="counter counter-md counter-inverse text-left">
	                <div class="counter-number-wrap font-size-30">
	                  <span class="counter-number">102</span>
	                  <span class="counter-number-related text-capitalize">Block</span>
	                </div>
	                <div class="counter-label text-capitalize">Total IPV6</div>
	              </div>
	            </div>
	            <!-- End Card -->
	          </div>
          	</div>

            <div class="row row-lg">
              <div class="col-lg-6">
                <!-- Example Area -->
                <div class="example-wrap">
                  <h4 class="example-title">Jumlah Dokumen Submit</h4>
                  <p></p>
                  <div class="example">
                    <div id="exampleMorrisArea"></div>
                  </div>
                </div>
                <!-- End Example Area -->
              </div>
            
              <div class="col-lg-6">
                <!-- Example Area -->
                <div class="example-wrap">
                  <h4 class="example-title">Jumlah BHP & USO</h4>
                  <p></p>
                  <div class="example">
                    <div id="exampleMorrisDonut"></div>
                  </div>
                </div>
                <!-- End Example Area -->
              </div>
            </div>

          </div>
         </div>


  </div>
</div>
<!-- End Page -->
<script src="<?php echo base_url(); ?>template/assets/global/vendor/raphael/raphael.min.js"></script>
<script src="<?php echo base_url(); ?>template/assets/global/vendor/morris/morris.min.js"></script>
<script>
(function () {
	Morris.Donut({
      element: 'exampleMorrisDonut',
      data: [{
        label: "BHP",
        value: 3500000000
      }, {
        label: "USO",
        value: 14800000000
      }],
      // barSizeRatio: 0.35,
      resize: true,
      colors: ['#3f51b5', '#4caf50']
    });

    Morris.Area({
      element: 'exampleMorrisArea',
      data: [{
        y: '01',
        a: 100,
        b: 80,
        c: 10,
        d: 20,
        e: 15
      }, {
        y: '02',
        a: 210,
        b: 110,
        c: 20,
        d: 20,
        e: 25
      }, {
        y: '03',
        a: 240,
        b: 130,
        c: 40,
        d: 20,
        e: 45
      }, {
        y: '04',
        a: 280,
        b: 250,
        c: 50,
        d: 70,
        e: 80
      }],
      xkey: 'y',
      ykeys: ['a', 'b','c','d','e'],
      labels: ['ISP', 'NAP','JARTUP', 'JARTAPLOK', 'JARTUPVSAT'],
      behaveLikeLine: true,
      ymax: 300,
      resize: true,
      pointSize: 3,
      smooth: true,
      gridTextColor: '#474e54',
      gridLineColor: '#eef0f2',
      goalLineColors: '#e3e6ea',
      gridTextFamily: 'fontFamily',
      gridTextWeight: '300',
      gridtextSize: 14,
      lineWidth: 1,
      fillOpacity: 0.1,
      lineColors: ['#3f51b5', '#4caf50','#26c6da','#ff9800','#f44336'],
      xLabelFormat: function (d) {
	    return "2017-" + d.getYear()
	  },
  	  
    });
  })();

</script>

