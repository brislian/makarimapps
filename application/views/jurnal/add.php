<div class="page">
  <div class="page-header">
    <h1 class="page-title">Tambah Jurnal Baru</h1>
    <div class="page-header-actions">
      <!-- <button type="button" class="btn btn-sm btn-icon btn-primary btn-round waves-effect waves-classic" data-toggle="tooltip" data-original-title="Edit">
        <i class="icon md-edit" aria-hidden="true"></i>
      </button>
      <button type="button" class="btn btn-sm btn-icon btn-primary btn-round waves-effect waves-classic" data-toggle="tooltip" data-original-title="Refresh">
        <i class="icon md-refresh-alt" aria-hidden="true"></i>
      </button>
      <button type="button" class="btn btn-sm btn-icon btn-primary btn-round waves-effect waves-classic" data-toggle="tooltip" data-original-title="Setting">
        <i class="icon md-settings" aria-hidden="true"></i>
      </button> -->
    </div>
  </div>
  <div class="page-content">
    <form action="<?php base_url()?>insert" method="post" id="thisform">
    <!-- Panel -->
    <div class="panel">
      <div class="panel-body">
        <div class="row">
          <div class="col-md-4">
            <div class="form-group">
                <h4 class="">Nama Jurnal</h4>
                <input type="text" class="form-control" name="accountname" autocapitalize="words">
            </div>
            <div class="form-group">
                <h4 class="">Tanggal Jurnal</h4>
                <div class="input-group">
                <input type="text" class="form-control" data-plugin="datepicker" name="accountdate" data-date-format='yyyy-mm-dd' >
                </div>
            </div>          
          </div>
          <div class="col-md-4">
              <blockquote class="blockquote custom-blockquote blockquote-success">
                <p class="mb-0 ">IDR <b class="totaldebit" id="totaldebit">0</b></p>
                <footer class="blockquote-footer">Total Debit
                </footer>
              </blockquote>
          </div>
          <div class="col-md-4">
              <blockquote class="blockquote custom-blockquote blockquote-warning">
                <p class="mb-0">IDR <b class="totalcredit" id="totalcredit">0</b></p>
                <footer class="blockquote-footer">Total Kredit
                </footer>
              </blockquote>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <table style="width:100%" class="table table-hover table-striped dataTable order-list" id="myTable">
                <thead>
                <tr>
                    <th style="width: 20%">Akun</th><th style="width: 20%">Jumlah</th><th>Deskripsi</th><th style="width: 10%">Tipe (D/C)</th>
                </tr>
                </thead>
                <tr> 
                    <td>
                      <select class="form-control" data-plugin="select2" name="accountcat0" >

                      <?php foreach ($data2->result() as $hasil) {
                        echo "<option value=$hasil->idcoa_no data-satu=$hasil->coa_category_idcoa_category_no>$hasil->coa_name</option>";
                      }

                      ?>
                      
                      </select>
                    </td>
                    <td>
                      <input type="text" class="form-control hitungtotal" name="accountamount0" id="accountamount0">
                    </td>
                    <td><input type="text" class="form-control" name="accountdesc0"></td>
                    <td>
                      <select class="form-control hitungtotal2" data-plugin="select2" name="accounttype0" id="accounttype0" >
                          <option value="D">Debit</option>
                          <option value="C">Kredit</option>
                      </select>
                    </td>
                    
                </tr>
            </table>
            <button type="button" class="btn btn-primary" id="addrow"><i class="icon wb-plus" aria-hidden="true"></i> Tambah Data</button>
            <button type="button" class="btn btn-icon btn-danger ibtnDel"><i class="icon wb-trash" aria-hidden="true"></i> Hapus Data</button>
          </div>
        </div>
        <!-- End Example Responsive -->
        <input type="hidden" id="iterasi" name="iterasi" value="0">
      </div>
    </div>
    <div class="form-group text-right">
        <button type="button" class="btn btn-warning waves-effect waves-classic" onclick="window.history.back()">Cancel</button>
        <button type="submit" class="btn btn-primary waves-effect waves-classic" id="btnsubmit">Submit </button>
    </div>
    
  </div>
</div>
</form>
<!-- End Page -->



<script type="text/javascript"> 

    var counter = 1;
   
    $("#addrow").on("click", function () {
        
        var newRow = $("<tr>");
        var cols = "";

        cols += '<td><select class="form-control" data-plugin="select2" name="accountcat' + counter + '" id="phone' + counter + '"></select></td>';
        cols += '<td><input type="text" class="form-control hitungtotal" name="accountamount' + counter + '" id="accountamount' + counter + '"/></td>';
        cols += '<td><input type="text" class="form-control" name="accountdesc' + counter + '"/></td>';
        cols += '<td><select class="form-control hitungtotal2" data-plugin="select2" name="accounttype' + counter + '" id="accounttype' + counter + '"><option value=D>Debit</option><option value=C>Kredit</option></select></td>';

        // cols += '<td><button type="button" class="btn btn-icon btn-danger ibtnDel" id="phone3' + counter + '"><i class="icon wb-trash" aria-hidden="true"></i></button></td>';
        newRow.append(cols);
        $("table.order-list").append(newRow);

        $("#phone2"+counter).select2();
        //$("#phone2"+counter).select2();
        $("#phone"+counter).select2({
            ajax: {
            url: '<?php echo base_url()?>Api/getcoa',
            dataType: 'json',
            delay: 250,
            data: function (params) {
                return {
                    q: params.term // search term
                };
            },
            processResults: function (data) {
                // parse the results into the format expected by Select2.
                // since we are using custom formatting functions we do not need to
                // alter the remote JSON data
                return {
                    results: data
                };
            },
            cache: true
        },
        // minimumInputLength: 2
        });

      $('#iterasi').val(counter);
      counter++;
        
    });

    $(".ibtnDel").on("click",function(){
        if(counter > 1){
          $('#myTable tr:last').remove();
          counter = counter-1;
          $('#iterasi').val(counter);
        }
    });






function calculateRow(row) {
    var price = +row.find('input[name^="price"]').val();

}

function calculateGrandTotal() {
    var grandTotal = 0;
    $("table.order-list").find('input[name^="price"]').each(function () {
        grandTotal += +$(this).val();
    });
    $("#grandtotal").text(grandTotal.toFixed(2));
}

$(document).on('keyup', ".hitungtotal",function () {
    var totaldebit = 0;
    var totalcredit = 0;
    for(i=0;i<counter;i++){
        var aamount = document.getElementById("accountamount"+i).value;
        var atype = document.getElementById("accounttype"+i).value;
        if(atype == "D"){          
          totaldebit = parseInt(totaldebit) + parseInt(aamount);  
    
        }
        if(atype == "C"){
          totalcredit = parseInt(totalcredit) + parseInt(aamount);
       
        }
    }
    $('.totaldebit').text(totaldebit);
    $('.totalcredit').text(totalcredit);
});

$(document).on('change', ".hitungtotal2",function () {
    var totaldebit = 0;
    var totalcredit = 0;
    for(i=0;i<counter;i++){
        var aamount = document.getElementById("accountamount"+i).value;
        var atype = document.getElementById("accounttype"+i).value;
        if(atype == "D"){          
          totaldebit = parseInt(totaldebit) + parseInt(aamount);  
    
        }
        if(atype == "C"){
          totalcredit = parseInt(totalcredit) + parseInt(aamount);
        
        }
    }
    $('.totaldebit').text(totaldebit);
    $('.totalcredit').text(totalcredit);
});





$('#btnsubmit').click(function() {
   cektipeakun();
   if ($('#accountnumber').val() !== "") {
     $.ajax({
        url: "<?php echo base_url()?>account/checkcoa",
        type: 'POST',
        data: { 'coa_id' : accountnum },
        success: function(result){
                   if(result > 0){
                      alertify.alert('Nomor Akun Sudah Pernah Ada');
                      $('#accountnumber').val('');
                   }
                   else{
                      $('#thisform').submit();
                      //alert(result);
                       // do something if username doesn't exist
                   }
                 }
        });
    }
});

</script>