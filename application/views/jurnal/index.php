<div class="page">
  <div class="page-header">
    <h1 class="page-title">Daftar Jurnal</h1>
    <div class="page-header-actions">
      <button type="button" class="btn btn-sm btn-icon btn-primary btn-round waves-effect waves-classic" data-toggle="tooltip" data-original-title="Tambah Akun" onclick="window.location.href=window.location.pathname+'/add'">
        <i class="icon md-plus" aria-hidden="true"></i>
      </button>
    </div>
  </div>
  <div class="page-content">
        <!-- Panel -->
        <div class="panel">
          <div class="panel-body">
            <div class="example-wrap">
              <div class="example">
                <div class="table-responsive">
                  <table class="table table-hover table-striped" cellspacing="0" id="exampleTableTools">
                    <thead>
                      <tr >
                        <th>Nomor Jurnal</th>
                        <th>Nama Jurnal</th>
                        <th>Tanggal Jurnal</th>
                        <th>Jumlah</th>
                        <th>Aksi</th>
                      </tr>
                    </thead>
                
                  </table>
                </div>
              </div>
            </div>
            <!-- End Example Responsive -->
           </div>
        </div>
  </div>
</div>
<!-- End Page -->
 
  

 	


<script type="text/javascript">      
  $('#exampleTableTools').DataTable({
  "paging": true,
  "lengthChange": false,
  "searching": false,
  "ordering": false,
  "info": true,
  "autoWidth": true
});

</script>

<script>
    toastr.options = {
      "closeButton": true,
      "debug": false,
      "newestOnTop": false,
      "progressBar": false,
      "positionClass": "toast-top-right",
      "preventDuplicates": false,
      "onclick": null,
      "showDuration": "300",
      "hideDuration": "500",
      "timeOut": "5000",
      "extendedTimeOut": "1000",
      "showEasing": "swing",
      "hideEasing": "linear",
      "showMethod": "fadeIn",
      "hideMethod": "fadeOut"
    }
</script>

<?php 
    if(isset($_GET['msg'])){
        if($_GET['msg']=="delsuccess"){ 
            echo  "<script>";
            echo   "toastr['success']('Data Berhasil dihapus');";
            echo  "</script>";
        } 
    }
?>