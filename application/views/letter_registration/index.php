<div class="page">
  <div class="page-header">
    <h1 class="page-title">Letter Registration</h1>
    <div class="page-header-actions">
      <button type="button" class="btn btn-sm btn-icon btn-primary btn-round waves-effect waves-classic" data-toggle="tooltip" data-original-title="Tambah Akun" onclick="window.location.href=window.location.pathname+'/add'">
        <i class="icon md-plus" aria-hidden="true"></i>
      </button>
    </div>
  </div>
  <div class="page-content">
        <!-- Panel -->
        <div class="panel">
          <div class="panel-body">
            <div class="example-wrap">
              <div class="example">
                <div class="table-responsive">
                  <table class="table table-hover table-striped" cellspacing="0" id="exampleTableTools">
                    <thead>
                      <tr>
                        <th>No.</th>
                        <th>Letter Date</th>
                        <th>Letter No</th>
                        <th>Client No</th>
                        <th>Month</th>
                        <th>Year</th>
                        <th>Partner</th>
                        <th>Staff 1</th>
                        <th>Staff 2</th>
                        <th>Secretary</th>
                        <th>To company</th>
                        <th>Address</th>
                        <th>Attention</th>
                        <th>Subject</th>
                        <th>Action</th>
                      </tr>
                    </thead>
                  <tbody>
                    <?php 
                      foreach($data1->result() as $hasil) { 
                        $id= $i;
                        
                        ?>
                        
                      <tr>
                        <td><?php echo $hasil->id_lett ;?></td>
                        <td><?php echo $hasil->date ;?></td>
                        <td><?php echo $hasil->letter_no ;?></td>
                        <td><?php echo $hasil->client_no; ?></td>
                        <td><?php echo $hasil->month; ?></td>
                        <td><?php echo $hasil->year; ?></td>
                        <td><?php echo $hasil->partner; ?></td>
                        <td><?php echo $hasil->staff1; ?></td>
                        <td><?php echo $hasil->staff2; ?></td>
                        <td><?php echo $hasil->secretary; ?></td>
                        <td><?php echo $hasil->comp_to; ?></td>
                        <td><?php echo $hasil->address; ?></td>
                        <td><?php echo $hasil->attention; ?></td>
                        <td><?php echo $hasil->subject; ?></td>
                        <td>
                        <button type="button" class="btn btn-icon btn-success" onclick="location.href='<?php echo base_url()."letter/edit/".$hasil->id_lett ?>'"><i class="icon md-edit" aria-hidden="true" "></i></button>
                        <button type="button" class="btn btn-icon btn-danger"" id="confirm" data-plugin="alertify"
                      data-type="confirm" data-confirm-title="Anda yakin Menghapus Data ini ?"
                      data-error-message="Dibatalkan" data-success-message="<?php echo base_url()."letter/delete/".$hasil->id_lett ?>"><i class="icon md-delete" aria-hidden="true" "></i></button>
                        </td>
                      </tr>
                     <?php } ?>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
            <!-- End Example Responsive -->
           </div>
        </div>
  </div>
</div>
<!-- End Page -->
 
  

  


<script type="text/javascript">      
  $('#exampleTableTools').DataTable({
  "paging": true,
  "lengthChange": false,
  "searching": false,
  "ordering": false,
  "info": true,
  "autoWidth": true
});

</script>

<script>
    toastr.options = {
      "closeButton": true,
      "debug": false,
      "newestOnTop": false,
      "progressBar": false,
      "positionClass": "toast-top-right",
      "preventDuplicates": false,
      "onclick": null,
      "showDuration": "300",
      "hideDuration": "500",
      "timeOut": "5000",
      "extendedTimeOut": "1000",
      "showEasing": "swing",
      "hideEasing": "linear",
      "showMethod": "fadeIn",
      "hideMethod": "fadeOut"
    }
</script>

<?php 
    if(isset($_GET['msg'])){
        if($_GET['msg']=="delsuccess"){ 
            echo  "<script>";
            echo   "toastr['success']('Data Berhasil dihapus');";
            echo  "</script>";
        } 
    }
?>