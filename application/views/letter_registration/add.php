<div class="page">
  <div class="page-header">
    <h1 class="page-title">Tambah Letter Registration</h1>
    <div class="page-header-actions">
      <!-- <button type="button" class="btn btn-sm btn-icon btn-primary btn-round waves-effect waves-classic" data-toggle="tooltip" data-original-title="Edit">
        <i class="icon md-edit" aria-hidden="true"></i>
      </button>
      <button type="button" class="btn btn-sm btn-icon btn-primary btn-round waves-effect waves-classic" data-toggle="tooltip" data-original-title="Refresh">
        <i class="icon md-refresh-alt" aria-hidden="true"></i>
      </button>
      <button type="button" class="btn btn-sm btn-icon btn-primary btn-round waves-effect waves-classic" data-toggle="tooltip" data-original-title="Setting">
        <i class="icon md-settings" aria-hidden="true"></i>
      </button> -->
    </div>
  </div>

  <div class="page-content">
    <!-- Panel -->
    <div class="panel">
      <div class="panel-body">
        <div class="col-md-4">
          <form action="<?php base_url()?>insert" method="post" id="thisform">
          
          <div class="form-group">
              <h4 class="">Tanggal</h4>
              <input type="date" class="form-control" name="date" >
          </div>

          <div class="form-group">
              <h4 class="">Letter No</h4>
                <input type="number" class="form-control" name="letterno" autocapitalize="words">
          </div>

          <div class="form-group">
              <h4 class="">Client No</h4>
                <input type="number" class="form-control" name="clientno">
          </div>

          <div class="form-group">
              <h4 class="">Month</h4>
              <input type="number" class="form-control" name="month">
          </div>

          <div class="form-group">
              <h4 class="">Year</h4>
              <input type="number" class="form-control" name="year">
          </div>
          
          <div class="form-group">
              <h4 class="">Partner</h4>
              <input type="text" class="form-control" name="partner">
          </div>

          <div class="form-group">
              <h4 class="">Staff 1</h4>
              <input type="text" class="form-control" name="staff1">
          </div>

          <div class="form-group">
              <h4 class="">Staff 2</h4>
              <input type="text" class="form-control" name="staff2" autocapitalize="words">
          </div>
          <div class="form-group">
              <h4 class="">Secretary</h4>
              <input type="text" class="form-control" name="sec" autocapitalize="words">
          </div>
            <div class="form-group">
              <h4 class="">To Company</h4>
              <input type="text" class="form-control" name="tocomp" autocapitalize="words">
          </div>

            <div class="form-group">
              <h4 class="">Address</h4>
              <textarea class="form-control" name="address" rows="5"></textarea>
          </div>
            <div class="form-group">
              <h4 class="">Attention</h4>
              <input type="text" class="form-control" name="att" autocapitalize="words">
          </div>
            <div class="form-group">
              <h4 class="">Subject</h4>
              <input type="text" class="form-control" name="subject" autocapitalize="words">
          </div>
      
               
        </div>
        <!-- End Example Responsive -->
      </div>
   
    </div>
    <div class="form-group text-right">
        <button type="button" class="btn btn-warning waves-effect waves-classic" onclick="window.history.back()">Cancel</button>
        <button type="submit" class="btn btn-primary waves-effect waves-classic" id="btnsubmit">Submit</button>
    </div>
     </form>
   
  </div>
</div>
<!-- End Page -->



<!-- <script type="text/javascript"> 
$('#turunanform').hide();

  $('#exampleTableTools').DataTable({
  "paging": true,
  "lengthChange": false,
  "searching": true,
  "ordering": true,
  "info": true,
  "autoWidth": false
});

function cektipeakun(){
  if($('#akunheaderyes').is(':checked')){
    a = "akunheader";
  } 
  if($('#akunheaderno').is(':checked')){
    a = "akunturunan";
  } 

      var acc = $('#accountnumber').val();
      if(acc == ""){
        alertify.theme('bootstrap');
        alertify.alert('Nomor Akun Belum Diisi');
        $('#akunheaderyes').prop('checked', false);
        $('#akunheaderno').prop('checked', false);

      } else {
          if (a == "akunheader") {
            accountnum =  $('#accountcategory').find(":selected").val() + "-" + $('#accountnumber').val();
            $('#turunanform').hide();
            $('#kategoriform').show();
            $('#prefix').text($('#accountcategory').find(":selected").val()+' - ');
          } else {
            accountnum =  $('#accountheader').find(":selected").data('satu') + "-" + $('#accountnumber').val();
            $('#turunanform').show();
            $('#kategoriform').hide();
            $('#prefix').text($('#accountheader').find(":selected").data('satu')+' - ');
          }
      }
}


$('#btnsubmit').click(function() {
   cektipeakun();
   if ($('#accountnumber').val() !== "") {
     $.ajax({
        url: "<?php echo base_url()?>account/checkcoa",
        type: 'POST',
        data: { 'coa_id' : accountnum },
        success: function(result){
                   if(result > 0){
                      alertify.alert('Nomor Akun Sudah Pernah Ada');
                      $('#accountnumber').val('');
                   }
                   else{
                      $('#thisform').submit();
                      //alert(result);
                       // do something if username doesn't exist
                   }
                 }
        });
    }
});
</script> -->