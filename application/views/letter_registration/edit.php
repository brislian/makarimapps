<div class="page">
  <div class="page-header">
    <h1 class="page-title">Ubah Data</h1>
    <div class="page-header-actions">
      <!-- <button type="button" class="btn btn-sm btn-icon btn-primary btn-round waves-effect waves-classic" data-toggle="tooltip" data-original-title="Edit">
        <i class="icon md-edit" aria-hidden="true"></i>
      </button>
      <button type="button" class="btn btn-sm btn-icon btn-primary btn-round waves-effect waves-classic" data-toggle="tooltip" data-original-title="Refresh">
        <i class="icon md-refresh-alt" aria-hidden="true"></i>
      </button>
      <button type="button" class="btn btn-sm btn-icon btn-primary btn-round waves-effect waves-classic" data-toggle="tooltip" data-original-title="Setting">
        <i class="icon md-settings" aria-hidden="true"></i>
      </button> -->
    </div>
  </div>
  <div class="page-content">
    <!-- Panel -->
    <div class="panel">
      <div class="panel-body">
        <div class="col-md-4">
          <form  method="post" id="thisform">
          <input type="hidden" name="flag" value="edit">
          <?php
              $hasil3 = $data3->result()[0];
          ?>
          <input type="hidden" name="idcoano" value="<?php echo $hasil3->id_lett ?>">
          <div class="form-group">
              <h4 class="">Letter Date</h4>
              <input type="date" class="form-control" name="date" autocapitalize="words" value="<?php echo $hasil3->date; ?>">
          </div>
        <div class="form-group">
              <h4 class="">Letter No</h4>
              <input type="number" class="form-control" name="letterno" autocapitalize="words" value="<?php echo $hasil3->letter_no; ?>">
          </div>
           <div class="form-group">
              <h4 class="">Client No</h4>
              <input type="number" class="form-control" name="clientno" autocapitalize="words" value="<?php echo $hasil3->client_no; ?>">
          </div>
           <div class="form-group">
              <h4 class="">Month</h4>
              <input type="number" class="form-control" name="month" autocapitalize="words" value="<?php echo $hasil3->month; ?>">
          </div>
           <div class="form-group">
              <h4 class="">Year</h4>
              <input type="number" class="form-control" name="year" autocapitalize="words" value="<?php echo $hasil3->year; ?>">
          </div>

          <div class="form-group">
              <h4 class="">Partner</h4>
             <input type="text" class="form-control" name="partner" autocapitalize="words" value="<?php echo $hasil3->partner; ?>">
             
          </div>

            <div class="form-group">
              <h4 class="">Staff 1</h4>
              <input type="text" class="form-control" name="staff1" autocapitalize="words" value="<?php echo $hasil3->staff1; ?>">
          </div>

            <div class="form-group">
              <h4 class="">Staff 2</h4>
              <input type="text" class="form-control" name="staff2" autocapitalize="words" value="<?php echo $hasil3->staff2; ?>">
          </div>

          <div class="form-group">
              <h4 class="">Secretary</h4>
              <input type="text" class="form-control" name="sec" autocapitalize="words" value="<?php echo $hasil3->secretary; ?>">
          </div>

          <div class="form-group">
              <h4 class="">To Company</h4>
              <input type="text" class="form-control" name="tocomp" autocapitalize="words" value="<?php echo $hasil3->comp_to; ?>">
          </div>

           <div class="form-group">
              <h4 class="">Address</h4>
               <textarea class="form-control" name="address" rows="5"><?php echo ($hasil3->address) ?></textarea>
          </div>

           <div class="form-group">
              <h4 class="">Attention</h4>
              <input type="text" class="form-control" name="att" autocapitalize="words" value="<?php echo $hasil3->attention; ?>">
          </div>
                
          <div class="form-group">
              <h4 class="">Subject</h4>
              <input type="text" class="form-control" name="subject" autocapitalize="words" value="<?php echo $hasil3->subject; ?>">
          </div>
        </div>
        <!-- End Example Responsive -->
      </div>
    </div>
    <div class="form-group text-right">
        <button type="button" class="btn btn-warning waves-effect waves-classic" onclick="window.history.back()">Cancel</button>
        <button type="submit" class="btn btn-primary waves-effect waves-classic" id="btnsubmit">Submit </button>
    </div>
    </form>
  </div>
</div>
<!-- End Page -->


