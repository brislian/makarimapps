
     
    </div>
  </div>
  <!-- End Page -->


 <!-- Core  -->
    <script src="<?php echo base_url(); ?>template/assets/global/vendor/babel-external-helpers/babel-external-helpers.js"></script>
    <script src="<?php echo base_url(); ?>template/assets/global/vendor/jquery/jquery.js"></script>
    <script src="<?php echo base_url(); ?>template/assets/global/vendor/popper-js/umd/popper.min.js"></script>
    <script src="<?php echo base_url(); ?>template/assets/global/vendor/bootstrap/bootstrap.js"></script>
    <script src="<?php echo base_url(); ?>template/assets/global/vendor/animsition/animsition.js"></script>
    <script src="<?php echo base_url(); ?>template/assets/global/vendor/mousewheel/jquery.mousewheel.js"></script>
    <script src="<?php echo base_url(); ?>template/assets/global/vendor/asscrollbar/jquery-asScrollbar.js"></script>
    <script src="<?php echo base_url(); ?>template/assets/global/vendor/asscrollable/jquery-asScrollable.js"></script>
    <script src="<?php echo base_url(); ?>template/assets/global/vendor/waves/waves.js"></script>
    
    <!-- Plugins -->
    <script src="<?php echo base_url(); ?>template/assets/global/vendor/switchery/switchery.js"></script>
    <script src="<?php echo base_url(); ?>template/assets/global/vendor/intro-js/intro.js"></script>
    <script src="<?php echo base_url(); ?>template/assets/global/vendor/screenfull/screenfull.js"></script>
    <script src="<?php echo base_url(); ?>template/assets/global/vendor/slidepanel/jquery-slidePanel.js"></script>
        <script src="<?php echo base_url(); ?>template/assets/global/vendor/jquery-placeholder/jquery.placeholder.js"></script>
    
    <!-- Scripts -->
    <script src="<?php echo base_url(); ?>template/assets/global/js/Component.js"></script>
    <script src="<?php echo base_url(); ?>template/assets/global/js/Plugin.js"></script>
    <script src="<?php echo base_url(); ?>template/assets/global/js/Base.js"></script>
    <script src="<?php echo base_url(); ?>template/assets/global/js/Config.js"></script>
    
    <script src="<?php echo base_url(); ?>template/assets/js/Section/Menubar.js"></script>
    <script src="<?php echo base_url(); ?>template/assets/js/Section/Sidebar.js"></script>
    <script src="<?php echo base_url(); ?>template/assets/js/Section/PageAside.js"></script>
    <script src="<?php echo base_url(); ?>template/assets/js/Plugin/menu.js"></script>
    
    <!-- Config -->
    <script src="<?php echo base_url(); ?>template/assets/global/js/config/colors.js"></script>
    <script src="<?php echo base_url(); ?>template/assets/js/config/tour.js"></script>
    <!-- <script>Config.set('<?php echo base_url(); ?>template/assets', '../../<?php echo base_url(); ?>template/assets');</script> -->
    
    <!-- Page -->
    <script src="<?php echo base_url(); ?>template/assets/js/Site.js"></script>
    <script src="<?php echo base_url(); ?>template/assets/global/js/Plugin/asscrollable.js"></script>
    <script src="<?php echo base_url(); ?>template/assets/global/js/Plugin/slidepanel.js"></script>
    <script src="<?php echo base_url(); ?>template/assets/global/js/Plugin/switchery.js"></script>
        <script src="<?php echo base_url(); ?>template/assets/global/js/Plugin/jquery-placeholder.js"></script>
        <script src="<?php echo base_url(); ?>template/assets/global/js/Plugin/material.js"></script>



  <script>
    (function(document, window, $) {
      'use strict';

      var Site = window.Site;
      $(document).ready(function() {
        Site.run();
      });
    })(document, window, jQuery);
  </script>

</body>


</html>