
  <!-- Core  -->
  <script src="<?php echo base_url(); ?>template/global/vendor/babel-external-helpers/babel-external-helpers.js"></script>
  <script src="<?php echo base_url(); ?>template/global/vendor/jquery/jquery.js"></script>
  <script src="<?php echo base_url(); ?>template/global/vendor/popper-js/umd/popper.min.js"></script>
  <script src="<?php echo base_url(); ?>template/global/vendor/bootstrap/bootstrap.js"></script>
  <script src="<?php echo base_url(); ?>template/global/vendor/animsition/animsition.js"></script>
  <script src="<?php echo base_url(); ?>template/global/vendor/mousewheel/jquery.mousewheel.js"></script>
  <script src="<?php echo base_url(); ?>template/global/vendor/asscrollbar/jquery-asScrollbar.js"></script>
  <script src="<?php echo base_url(); ?>template/global/vendor/asscrollable/jquery-asScrollable.js"></script>
  
  <!-- Plugins -->
  <script src="<?php echo base_url(); ?>template/global/vendor/switchery/switchery.js"></script>
  <script src="<?php echo base_url(); ?>template/global/vendor/intro-js/intro.js"></script>
  <script src="<?php echo base_url(); ?>template/global/vendor/screenfull/screenfull.js"></script>
  <script src="<?php echo base_url(); ?>template/global/vendor/slidepanel/jquery-slidePanel.js"></script>
  <script src="<?php echo base_url(); ?>template/global/vendor/select2/select2.full.min.js"></script>
  <script src="<?php echo base_url(); ?>template/global/vendor/bootstrap-tokenfield/bootstrap-tokenfield.min.js"></script>
  <script src="<?php echo base_url(); ?>template/global/vendor/bootstrap-tagsinput/bootstrap-tagsinput.min.js"></script>
  <script src="<?php echo base_url(); ?>template/global/vendor/bootstrap-select/bootstrap-select.js"></script>
  <script src="<?php echo base_url(); ?>template/global/vendor/icheck/icheck.min.js"></script>
  <script src="<?php echo base_url(); ?>template/global/vendor/switchery/switchery.js"></script>
  <script src="<?php echo base_url(); ?>template/global/vendor/asrange/jquery-asRange.min.js"></script>
  <script src="<?php echo base_url(); ?>template/global/vendor/ionrangeslider/ion.rangeSlider.min.js"></script>
  <script src="<?php echo base_url(); ?>template/global/vendor/asspinner/jquery-asSpinner.min.js"></script>
  <script src="<?php echo base_url(); ?>template/global/vendor/clockpicker/bootstrap-clockpicker.min.js"></script>
  <script src="<?php echo base_url(); ?>template/global/vendor/ascolor/jquery-asColor.min.js"></script>
  <script src="<?php echo base_url(); ?>template/global/vendor/asgradient/jquery-asGradient.min.js"></script>
  <script src="<?php echo base_url(); ?>template/global/vendor/ascolorpicker/jquery-asColorPicker.min.js"></script>
  <script src="<?php echo base_url(); ?>template/global/vendor/bootstrap-maxlength/bootstrap-maxlength.js"></script>
  <script src="<?php echo base_url(); ?>template/global/vendor/jquery-knob/jquery.knob.js"></script>
  <script src="<?php echo base_url(); ?>template/global/vendor/bootstrap-touchspin/bootstrap-touchspin.min.js"></script>
  <script src="<?php echo base_url(); ?>template/global/vendor/jquery-labelauty/jquery-labelauty.js"></script>
  <script src="<?php echo base_url(); ?>template/global/vendor/bootstrap-datepicker/bootstrap-datepicker.js"></script>
  <script src="<?php echo base_url(); ?>template/global/vendor/timepicker/jquery.timepicker.min.js"></script>
  <script src="<?php echo base_url(); ?>template/global/vendor/datepair/datepair.min.js"></script>
  <script src="<?php echo base_url(); ?>template/global/vendor/datepair/jquery.datepair.min.js"></script>
  <script src="<?php echo base_url(); ?>template/global/vendor/jquery-strength/password_strength.js"></script>
  <script src="<?php echo base_url(); ?>template/global/vendor/jquery-strength/password_strength.js"></script>
  <script src="<?php echo base_url(); ?>template/global/vendor/jquery-strength/jquery-strength.min.js"></script>
  <script src="<?php echo base_url(); ?>template/global/vendor/multi-select/jquery.multi-select.js"></script>
  <script src="<?php echo base_url(); ?>template/global/vendor/typeahead-js/bloodhound.min.js"></script>
  <script src="<?php echo base_url(); ?>template/global/vendor/typeahead-js/typeahead.jquery.min.js"></script>
  <script src="<?php echo base_url(); ?>template/global/vendor/jquery-placeholder/jquery.placeholder.js"></script>
    
  <!-- Scripts -->
  <script src="<?php echo base_url(); ?>template/global/js/Component.js"></script>
  <script src="<?php echo base_url(); ?>template/global/js/Plugin.js"></script>
  <script src="<?php echo base_url(); ?>template/global/js/Base.js"></script>
  <script src="<?php echo base_url(); ?>template/global/js/Config.js"></script>
  
  <script src="<?php echo base_url(); ?>template/assets/js/Section/Menubar.js"></script>
  <script src="<?php echo base_url(); ?>template/assets/js/Section/Sidebar.js"></script>
  <script src="<?php echo base_url(); ?>template/assets/js/Section/PageAside.js"></script>
  <script src="<?php echo base_url(); ?>template/assets/js/Plugin/menu.js"></script>
  
  <!-- Config -->
  <script src="<?php echo base_url(); ?>template/global/js/config/colors.js"></script>
  <script src="<?php echo base_url(); ?>template/assets/js/config/tour.js"></script>
  <script>Config.set('assets', '<?php echo base_url(); ?>template/assets');</script>
  
  <!-- Page -->
  <script src="<?php echo base_url(); ?>template/assets/js/Site.js"></script>
  <script src="<?php echo base_url(); ?>template/global/js/Plugin/asscrollable.js"></script>
  <script src="<?php echo base_url(); ?>template/global/js/Plugin/slidepanel.js"></script>
  <script src="<?php echo base_url(); ?>template/global/js/Plugin/switchery.js"></script>
  <script src="<?php echo base_url(); ?>template/global/js/Plugin/select2.js"></script>
  <script src="<?php echo base_url(); ?>template/global/js/Plugin/bootstrap-tokenfield.js"></script>
  <script src="<?php echo base_url(); ?>template/global/js/Plugin/bootstrap-tagsinput.js"></script>
  <script src="<?php echo base_url(); ?>template/global/js/Plugin/bootstrap-select.js"></script>
  <script src="<?php echo base_url(); ?>template/global/js/Plugin/icheck.js"></script>
  <script src="<?php echo base_url(); ?>template/global/js/Plugin/switchery.js"></script>
  <script src="<?php echo base_url(); ?>template/global/js/Plugin/asrange.js"></script>
  <script src="<?php echo base_url(); ?>template/global/js/Plugin/ionrangeslider.js"></script>
  <script src="<?php echo base_url(); ?>template/global/js/Plugin/asspinner.js"></script>
  <script src="<?php echo base_url(); ?>template/global/js/Plugin/clockpicker.js"></script>
  <script src="<?php echo base_url(); ?>template/global/js/Plugin/ascolorpicker.js"></script>
  <script src="<?php echo base_url(); ?>template/global/js/Plugin/bootstrap-maxlength.js"></script>
  <script src="<?php echo base_url(); ?>template/global/js/Plugin/jquery-knob.js"></script>
  <script src="<?php echo base_url(); ?>template/global/js/Plugin/bootstrap-touchspin.js"></script>
  <script src="<?php echo base_url(); ?>template/global/js/Plugin/card.js"></script>
  <script src="<?php echo base_url(); ?>template/global/js/Plugin/jquery-labelauty.js"></script>
  <script src="<?php echo base_url(); ?>template/global/js/Plugin/bootstrap-datepicker.js"></script>
  <script src="<?php echo base_url(); ?>template/global/js/Plugin/jt-timepicker.js"></script>
  <script src="<?php echo base_url(); ?>template/global/js/Plugin/datepair.js"></script>
  <script src="<?php echo base_url(); ?>template/global/js/Plugin/jquery-strength.js"></script>
  <script src="<?php echo base_url(); ?>template/global/js/Plugin/multi-select.js"></script>
  <script src="<?php echo base_url(); ?>template/global/js/Plugin/jquery-placeholder.js"></script>

  <!-- <script src="<?php echo base_url(); ?>template/assets/examples/js/forms/advanced.js"></script> -->

  <script src="<?php echo base_url(); ?>template/global/vendor/datatables.net/jquery.dataTables.js"></script>
  <script src="<?php echo base_url(); ?>template/global/vendor/datatables.net-bs4/dataTables.bootstrap4.js"></script>
  <script src="<?php echo base_url(); ?>template/global/vendor/datatables.net-fixedheader/dataTables.fixedHeader.js"></script>
  <script src="<?php echo base_url(); ?>template/global/vendor/datatables.net-fixedcolumns/dataTables.fixedColumns.js"></script>
  <script src="<?php echo base_url(); ?>template/global/vendor/datatables.net-rowgroup/dataTables.rowGroup.js"></script>
  <script src="<?php echo base_url(); ?>template/global/vendor/datatables.net-scroller/dataTables.scroller.js"></script>
  <script src="<?php echo base_url(); ?>template/global/vendor/datatables.net-responsive/dataTables.responsive.js"></script>
  <script src="<?php echo base_url(); ?>template/global/vendor/datatables.net-responsive-bs4/responsive.bootstrap4.js"></script>
  <script src="<?php echo base_url(); ?>template/global/vendor/datatables.net-buttons/dataTables.buttons.js"></script>
  <script src="<?php echo base_url(); ?>template/global/vendor/datatables.net-buttons/buttons.html5.js"></script>
  <script src="<?php echo base_url(); ?>template/global/vendor/datatables.net-buttons/buttons.flash.js"></script>
  <script src="<?php echo base_url(); ?>template/global/vendor/datatables.net-buttons/buttons.print.js"></script>
  <script src="<?php echo base_url(); ?>template/global/vendor/datatables.net-buttons/buttons.colVis.js"></script>
  <script src="<?php echo base_url(); ?>template/global/vendor/datatables.net-buttons-bs4/buttons.bootstrap4.js"></script>
  <script src="<?php echo base_url(); ?>template/global/vendor/asrange/jquery-asRange.min.js"></script>
  <script src="<?php echo base_url(); ?>template/global/vendor/bootbox/bootbox.js"></script>
  <script src="<?php echo base_url(); ?>template/global/vendor/toastr/toastr.js"></script>
  <script src="<?php echo base_url(); ?>template/global/js/Plugin/toastr.js"></script>
  <script src="<?php echo base_url(); ?>template/global/vendor/alertify/alertify.js"></script>
  <script src="<?php echo base_url(); ?>template/global/vendor/notie/notie.js"></script>
  <script src="<?php echo base_url(); ?>template/global/js/Plugin/alertify.js"></script>
  <script src="<?php echo base_url(); ?>template/global/js/Plugin/notie-js.js"></script>

