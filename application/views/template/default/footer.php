 
 <!-- Footer -->
    <footer class="site-footer">
      <div class="site-footer-legal">© 2018 <a href="#">BSM</a></div>
      
    </footer>
    
    <script>
      (function(document, window, $){
        'use strict';
    
        var Site = window.Site;
        $(document).ready(function(){
          Site.run();
        });
      })(document, window, jQuery);
    </script>
    
  

    <!-- <script type="text/javascript">
        (function ($) {
            $(document).ready(function () {
                $('.spinner-bg').fadeOut();
                $('.spinner-img').fadeOut();
                
            });

        })(jQuery);



    </script> 
         -->
  </body>
</html>
