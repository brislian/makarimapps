
<!DOCTYPE html>
<html class="no-js css-menubar" lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="bootstrap material admin template">
    <meta name="author" content="">
    
   <title>Makarim</title>
   
    <!-- <link rel="apple-touch-icon" href="<?php echo base_url(); ?>template/images/logo/logo1.jpg">
    <link rel="shortcut icon" href="<?php echo base_url(); ?>template/images/logo/logo1.jpg">
     -->
    <!-- Stylesheets -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>template/global/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>template/global/css/bootstrap-extend.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>template/assets/css/site.min.css">
    
    <!-- Plugins -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>template/global/vendor/animsition/animsition.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>template/global/vendor/asscrollable/asScrollable.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>template/global/vendor/switchery/switchery.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>template/global/vendor/intro-js/introjs.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>template/global/vendor/slidepanel/slidePanel.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>template/global/vendor/flag-icon-css/flag-icon.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>template/global/vendor/select2/select2.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>template/global/vendor/bootstrap-tokenfield/bootstrap-tokenfield.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>template/global/vendor/bootstrap-tagsinput/bootstrap-tagsinput.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>template/global/vendor/bootstrap-select/bootstrap-select.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>template/global/vendor/icheck/icheck.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>template/global/vendor/switchery/switchery.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>template/global/vendor/asrange/asRange.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>template/global/vendor/ionrangeslider/ionrangeslider.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>template/global/vendor/asspinner/asSpinner.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>template/global/vendor/clockpicker/clockpicker.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>template/global/vendor/ascolorpicker/asColorPicker.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>template/global/vendor/bootstrap-touchspin/bootstrap-touchspin.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>template/global/vendor/jquery-labelauty/jquery-labelauty.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>template/global/vendor/bootstrap-datepicker/bootstrap-datepicker.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>template/global/vendor/bootstrap-maxlength/bootstrap-maxlength.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>template/global/vendor/timepicker/jquery-timepicker.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>template/global/vendor/jquery-strength/jquery-strength.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>template/global/vendor/multi-select/multi-select.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>template/global/vendor/typeahead-js/typeahead.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>template/assets/examples/css/forms/advanced.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>template/global/vendor/toastr/toastr.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>template/assets/examples/css/advanced/toastr.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>template/global/vendor/alertify/alertify.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>template/global/vendor/notie/notie.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>template/assets/examples/css/advanced/alertify.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>template/global/vendor/morris/morris.css">
    
    
    <!-- Fonts -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>template/global/fonts/web-icons/web-icons.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>template/global/fonts/material-design/material-design.min.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>template/global/fonts/brand-icons/brand-icons.min.css">
    <link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,300italic'>
    
    <!-- Scripts -->
    <script src="<?php echo base_url(); ?>template/global/vendor/breakpoints/breakpoints.js"></script>
    <script>
      Breakpoints();
    </script>
	   
    <style type="text/css">
      table.dataTable thead tr {
        background-color: #17a2b8; 
      }
      table.dataTable thead tr th{
        color: #fff; 
      }
    </style>

  </head>
  <body class="animsition site-navbar-small">
    <!-- <div class="spinner-bg" style="position: absolute; top: 0; left: 0; width: 100%; height: 100%; background-color: #333333; opacity: 0.3; z-index: 9999;"></div>
    <div class="spinner-img" style="position: absolute; top: 45%; left: 45%; z-index: 99999;">
    	<div align="center"><img src="<?php echo base_url() ?>template/global/vendor/images/blueimp-file-upload/loading.gif"/></div>
    </div> -->

    <nav class="site-navbar navbar navbar-default navbar-fixed-top navbar-mega" role="navigation" style="background: #17a2b8 !important;color:">
    
      <div class="navbar-header">
       <button type="button" class="navbar-toggler hamburger hamburger-close navbar-toggler-left hided"
          data-toggle="menubar">
          <span class="sr-only">Toggle navigation</span>
          <span class="hamburger-bar"></span>
        </button>
        <button type="button" class="navbar-toggler collapsed" data-target="#site-navbar-collapse"
          data-toggle="collapse" style="background-color: #FFB600;">
          <i class="icon md-more" aria-hidden="true"></i>
        </button>
        <a class="navbar-brand navbar-brand-center" href="home">
          <img class="navbar-brand-logo navbar-brand-logo-normal" src="<?php echo base_url()?>template/assets/images/logo-blue.png" 
            title="" >
          <img class="navbar-brand-logo navbar-brand-logo-special" src="<?php echo base_url()?>template/assets/images/logo-blue.png"
              title="" >
        <span class="navbar-brand-text hidden-xs text-white">Makarim</span>
        </a>
        <button type="button" class="navbar-toggler collapsed" data-target="#site-navbar-search"
          data-toggle="collapse">
          <span class="sr-only">Toggle Search</span>
          <i class="icon md-search" aria-hidden="true"></i>
        </button>
      </div>
    
      <div class="navbar-container container-fluid">
        <!-- Navbar Collapse -->
        <div class="collapse navbar-collapse navbar-collapse-toolbar" id="site-navbar-collapse">
          <!-- Navbar Toolbar -->
          <ul class="nav navbar-toolbar">
            <li class="nav-item hidden-float" id="toggleMenubar">
              <a class="nav-link" data-toggle="menubar" href="#" role="button">
                <i class="icon hamburger hamburger-arrow-left">
                  <span class="sr-only">Toggle menubar</span>
                  <span class="hamburger-bar"></span>
                </i>
              </a>
            </li>
            <li class="nav-item hidden-sm-down" id="toggleFullscreen">
              <a class="nav-link icon icon-fullscreen" data-toggle="fullscreen" href="#" role="button">
                <span class="sr-only">Toggle fullscreen</span>
              </a>
            </li>
          </ul>
          <!-- End Navbar Toolbar -->
    
          <!-- Navbar Toolbar Right -->
          <ul class="nav navbar-toolbar navbar-right navbar-toolbar-right" style=" margin-right: -15px; ">
            <?php if ($this->session->userdata("group_kode_tipeakun") == 'Cabang') { ?> 
              <li id="responsecontainer" class="nav-item dropdown" style="margin-top: 20px; margin-right: 9px;">
                <?php
                  $this->load->view('notification');
                ?>
              </li>
            <?php } ?>

             <li class="nav-item dropdown" >
              <a class="nav-link navbar-avatar" data-toggle="dropdown" href="#" aria-expanded="false"
                data-animation="scale-up" role="button" style="padding-right: 100px; ">
                <span class="avatar avatar-online text-white">
                  <?php $poto = $this->session->userdata("poto");
                    $full_path = "assets/images/user/ss_" . $poto;
                    if (!file_exists($full_path) || $poto == "") $img_type = 0; else $img_type = 1;
                    
                    if ($img_type == 1) {
                  ?>
                    <img src="<?php echo base_url()?>template/assets/images/user/ss_<?php echo $poto ?>"  alt="..." style="height: 30px; width: 30px">
                  <?php } else { ?>
                    <img src="<?php echo base_url()?>template/assets/images/avatar.jpg" class="img-rounded" alt="...">
                          <?php } ?><?php $fullname = $this->session->userdata("username"); ?>
                          &nbsp<?php echo "   ".$fullname ?>
      
                      </span>
                    </a>
                    <div class="dropdown-menu" role="menu">
                      <a href="profile" role="menuitem" class="dropdown-item"><i class="icon md-account" aria-hidden="true"></i> Profile</a>
            
                      <?php if ($this->session->userdata("group_kode_tipeakun") == 'Cabang') { ?>
                      <a href="fee" role="menuitem" class="dropdown-item" ><i class="icon md-money" aria-hidden="true"></i> Rp.
                         <?php
                         echo number_format($this->ttQ[0]->total,2,',','.'); 
                          ?> 
                      </a>
                    <?php } ?>
                    <div class="dropdown-divider"></div>
                      <a href="<?php echo base_url(); ?>login/logout" role="menuitem" class="dropdown-item"><i class="icon md-power" aria-hidden="true"></i> Keluar</a>
                    <!-- </li> -->
                    </div>                   
            </li>
            
            
          </ul>
          <!-- End Navbar Toolbar Right -->
        </div>
        <!-- End Navbar Collapse -->
    
        <!-- Site Navbar Seach -->
        <div class="collapse navbar-search-overlap" id="site-navbar-search">
          <form role="search">
            <div class="form-group">
              <div class="input-search">
                <i class="input-search-icon md-search" aria-hidden="true"></i>
                <input type="text" class="form-control" name="site-search" placeholder="Search...">
                <button type="button" class="input-search-close icon md-close" data-target="#site-navbar-search"
                  data-toggle="collapse" aria-label="Close"></button>
              </div>
            </div>
          </form>
        </div>
        <!-- End Site Navbar Seach -->
      </div>
    </nav>    


<div class="site-menubar site-menubar-light">
  <div class="site-menubar-body">
    <div>
      <div>
        <ul class="site-menu" data-plugin="menu">
          <li class="site-menu-category">Menu</li>
          <li class="site-menu-item">
            <a class="animsition-link" href="<?php echo base_url() ?>">
              <i class="site-menu-icon md-home" aria-hidden="true"></i>
              <span class="site-menu-title">Dashboard</span>
            </a>
          </li>
          <li class="site-menu-item">
            <a href="<?php echo base_url() ?>account">
              <i class="site-menu-icon md-library" aria-hidden="true"></i>
              <span class="site-menu-title">Daftar Akun</span>
            </a>
          </li>
          <li class="site-menu-item">
            <a href="<?php echo base_url() ?>contact_program">
              <i class="site-menu-icon md-library" aria-hidden="true"></i>
              <span class="site-menu-title">Contact Program</span>
            </a>
          </li>
        <!--   <li class="dropdown site-menu-item has-sub">
            <a data-toggle="dropdown" href="javascript:void(0)" data-dropdown-toggle="false">
                    <i class="site-menu-icon wb-extension" aria-hidden="true"></i>
                    <span class="site-menu-title">Employee</span>
                        <span class="site-menu-arrow"></span>
                </a>
            <div class="dropdown-menu">
              <div class="site-menu-scroll-wrap is-list">
                <div>
                  <div>
                    <ul class="site-menu-sub site-menu-normal-list">
                      <li class="site-menu-item">
                        <a class="animsition-link" href="<?php echo base_url() ?>report/trialbalance">
                          <span class="site-menu-title">Laporan Saldo</span>
                        </a>
                      </li>
                      <li class="site-menu-item">
                        <a class="animsition-link" href="<?php echo base_url() ?>report/profitloss">
                          <span class="site-menu-title">Laporan Laba Rugi</span>
                        </a>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </li> -->

        <!--     <li class="dropdown site-menu-item has-sub">
            <a data-toggle="dropdown" href="javascript:void(0)" data-dropdown-toggle="false">
                    <i class="site-menu-icon wb-extension" aria-hidden="true"></i>
                    <span class="site-menu-title">Attendance</span>
                        <span class="site-menu-arrow"></span>
                </a>
            <div class="dropdown-menu">
              <div class="site-menu-scroll-wrap is-list">
                <div>
                  <div>
                    <ul class="site-menu-sub site-menu-normal-list">
                      <li class="site-menu-item">
                        <a class="animsition-link" href="<?php echo base_url() ?>report/trialbalance">
                          <span class="site-menu-title">Attendance Report</span>
                        </a>
                      </li>
                      <li class="site-menu-item">
                        <a class="animsition-link" href="<?php echo base_url() ?>report/profitloss">
                          <span class="site-menu-title">Shift Report</span>
                        </a>
                      </li>
                      <li class="site-menu-item">
                        <a class="animsition-link" href="<?php echo base_url() ?>report/profitloss">
                          <span class="site-menu-title">Update Attandance</span>
                        </a>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </li> -->

            <li class="site-menu-item">
            <a href="<?php echo base_url() ?>letter">
              <i class="site-menu-icon md-library" aria-hidden="true"></i>
              <span class="site-menu-title">Letter Registration Number</span>
            </a>
          </li>
        </ul>
      </div>
    </div>
  </div>
</div>