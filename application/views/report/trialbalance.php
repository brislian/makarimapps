<div class="page">
  <div class="page-header">
    <h1 class="page-title">Laporan Saldo</h1>
    <div class="page-header-actions">
      <button type="button" class="btn btn-sm btn-icon btn-primary btn-round waves-effect waves-classic" data-toggle="tooltip" data-original-title="Tambah Akun" onclick="window.location.href=window.location.pathname+'/add'">
        <i class="icon md-plus" aria-hidden="true"></i>
      </button>
    </div>
  </div>
  <div class="page-content">
        <!-- Panel -->
        <div class="panel">
          <div class="panel-body">
            <form action="<?php base_url()."report/trialbalance" ?>" method="POST">
              <div class="row">
                <div class="col-md-4">
                  <div class="form-group">
                    <h4 class="">Range Data</h4>
                    <div class="input-daterange" data-plugin="datepicker">
                      <div class="input-group">
                        <span class="input-group-addon">
                          <i class="icon wb-calendar" aria-hidden="true"></i>
                        </span>
                        <input type="text" class="form-control datepicker" name="datefrom" data-date-format='yyyy-mm-dd'>
                      </div>
                      <div class="input-group">
                        <span class="input-group-addon">to</span>
                        <input type="text" class="form-control datepicker" name="dateto" data-date-format='yyyy-mm-dd'>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <br>
              <button type="submit" class="btn btn-primary waves-effect waves-classic" id="btnsubmit">Submit</button>
              </form>
              <br><br>
              <?php if(!empty($data1->result())) { ?>
              <div class="row">
                <div class="col-md-12">
                  <table style="width:100%" class="table table-hover table-striped dataTable order-list" id="myTable">
                    <thead class="table-header report-header">
                      <tr>
                        <th class="border-bottom border-sides in-the-middle" colspan="2" rowspan="2">
                        Daftar Akun
                        </th>
                        <th class="text-center border-bottom border-sides" colspan="2">
                        Saldo Awal
                        </th>
                        <th class="text-center border-bottom border-sides" colspan="2">
                        Saldo Akhir
                        </th>
                        </tr>
                        <tr>
                        <th class="text-center border-bottom border-sides">
                        Debit
                        </th>
                        <th class="text-center border-bottom border-sides">
                        Credit
                        </th>
                        <th class="text-center border-bottom border-sides">
                        Debit
                        </th>
                        <th class="text-center border-bottom border-sides">
                        Credit
                        </th>
                      </tr>
                    </thead>
                    <tbody>
                    <?php 
                    	foreach($data1->result() as $hasil) { 
                    		$id= $i;
                    	//$id = $this->globalfunction->base64_encrypt($i);
                        $url = $this->globalfunction->base64_encrypt($hasil->jurnal_idjurnal);
                        $a = $hasil->amountcreditawal;
                        $b = $hasil->amountdebitawal;
                        $c = $b - $a;
                        
                        if($c < 0){
                          $d = abs($c);
                          $c = 0;
                        } else {
                          $d = 0;
                          
                        }
                        $c2 = $this->globalfunction->konversirupiah($c);
                        $d2 = $this->globalfunction->konversirupiah($d);

                    		?>
                      <tr>
                        <td class="text-right"><?php echo $hasil->coa_id; ?></td>
                        <td><?php echo $hasil->coa_name; ?></td>
                        <td class="text-right"><?php  echo $c2; ?></td>
                        <td class="text-right"><?php  echo $d2; ?></td>
                        <td class="text-right"><?php echo $this->globalfunction->konversirupiah($hasil->amountcredit); ?></td>
                        <td class="text-right"><?php echo $this->globalfunction->konversirupiah($hasil->amountdebit); ?></td>
                        
                      </tr>
                     <?php } ?>
                    </tbody>
                  </table>
                </div>
              </div>
              <?php  } ?>
              </div>
            </div>
            <!-- End Example Responsive -->
           </div>
        </div>
  </div>
</div>
<!-- End Page -->
 
  

 	


<script type="text/javascript">      
  $('#exampleTableTools').DataTable({
  "paging": true,
  "lengthChange": false,
  "searching": false,
  "ordering": false,
  "info": true,
  "autoWidth": true
});

  $('.datepicker').datepicker({
    orientation: 'bottom'
})

</script>

<script>
    toastr.options = {
      "closeButton": true,
      "debug": false,
      "newestOnTop": false,
      "progressBar": false,
      "positionClass": "toast-top-right",
      "preventDuplicates": false,
      "onclick": null,
      "showDuration": "300",
      "hideDuration": "500",
      "timeOut": "5000",
      "extendedTimeOut": "1000",
      "showEasing": "swing",
      "hideEasing": "linear",
      "showMethod": "fadeIn",
      "hideMethod": "fadeOut"
    }
</script>

<?php 
    if(isset($_GET['msg'])){
        if($_GET['msg']=="delsuccess"){ 
            echo  "<script>";
            echo   "toastr['success']('Data Berhasil dihapus');";
            echo  "</script>";
        } 
    }
?>