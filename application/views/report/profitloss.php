<div class="page">
  <div class="page-header">
    <h1 class="page-title">Laba Rugi</h1>
    <div class="page-header-actions">
      <button type="button" class="btn btn-sm btn-icon btn-primary btn-round waves-effect waves-classic" data-toggle="tooltip" data-original-title="Tambah Akun" onclick="window.location.href=window.location.pathname+'/add'">
        <i class="icon md-plus" aria-hidden="true"></i>
      </button>
    </div>
  </div>
  <div class="page-content">
        <!-- Panel -->
        <div class="panel">
          <div class="panel-body">
            <form action="<?php base_url()."report/profitloss" ?>" method="POST">
              <div class="row">
                <div class="col-md-4">
                  <div class="form-group">
                    <h4 class="">Range Data</h4>
                    <div class="input-daterange" data-plugin="datepicker">
                      <div class="input-group">
                        <span class="input-group-addon">
                          <i class="icon wb-calendar" aria-hidden="true"></i>
                        </span>
                        <input type="text" class="form-control datepicker" name="datefrom" data-date-format='yyyy-mm-dd'>
                      </div>
                      <div class="input-group">
                        <span class="input-group-addon">to</span>
                        <input type="text" class="form-control datepicker" name="dateto" data-date-format='yyyy-mm-dd'>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <br>
              <button type="submit" class="btn btn-primary waves-effect waves-classic" id="btnsubmit">Submit</button>
              </form>
              <br><br>
              <?php if(!empty($data1->result())) { ?>
              <div class="row">
                <div class="col-md-12">
                  <table style="width:100%" class="table table-hover table-striped dataTable order-list" id="myTable">
                    <thead class="table-header report-header">
                      <tr>
                        <th class="border-bottom border-sides in-the-middle" style="width: 50%">
                        Akun
                        </th>
                        <th class="text-center border-bottom border-sides">
                        Saldo Akhir
                        </th>
                        
                      </tr>
                    </thead>
                    <tbody>

                    <?php 
                    	foreach($data1->result() as $hasil) { 
                        ?>
                      <tr>
                        <?php if($hasil->coa_id == "a") {echo "<td><b>".$hasil->coa_category_name."</b></td>";} else {echo "<td style='padding-left:20px'>".$hasil->coa_name."</td>"; } ?></td>
                        <td><?php echo $hasil->jumlah1; ?></td>
                      </tr>
                     <?php } ?>
                    </tbody>
                    <thead class="table-header report-header">
                      <tr style="background-color: green !important">
                        <th class="border-bottom border-sides in-the-middle">
                        Total
                        </th>
                        <th class="text-center border-bottom border-sides">
                        
                        </th>
                      </tr>
                    </thead>
                  </table>
                  <table style="width:100%" class="table table-hover table-striped dataTable order-list" id="myTable">
                    <thead class="table-header report-header">
                      <tr>
                        <th class="border-bottom border-sides in-the-middle" style="width: 50%">
                        Akun
                        </th>
                        <th class="text-center border-bottom border-sides">
                        Saldo Akhir
                        </th>
                        
                      </tr>
                    </thead>
                    <tbody>

                    <?php 
                      foreach($data2->result() as $hasil2) { 
                        ?>
                      <tr>
                        <?php if($hasil2->coa_id == "a") {echo "<td><b>".$hasil2->coa_category_name."</b></td>";} else {echo "<td style='padding-left:20px'>".$hasil2->coa_name."</td>"; } ?></td>
                        <td><?php echo $hasil2->jumlah1; ?></td>
                      </tr>
                     <?php } ?>
                    </tbody>
                    <thead class="table-header report-header">
                       <tr style="background-color: green !important">
                        <th class="border-bottom border-sides in-the-middle">
                        Total
                        </th>
                        <th class="text-center border-bottom border-sides">
                        
                        </th>
                      </tr>
                    </thead>
                  </table>
                  <table style="width:100%" class="table table-hover table-striped dataTable order-list" id="myTable">
                    <thead class="table-header report-header">
                      <tr>
                        <th class="border-bottom border-sides in-the-middle" style="width: 50%">
                        Akun
                        </th>
                        <th class="text-center border-bottom border-sides">
                        Saldo Akhir
                        </th>
                        
                      </tr>
                    </thead>
                    <tbody>

                    <?php 
                      foreach($data3->result() as $hasil3) { 
                        ?>
                      <tr>
                        <?php if($hasil3->coa_id == "a") {echo "<td><b>".$hasil3->coa_category_name."</b></td>";} else {echo "<td style='padding-left:20px'>".$hasil3->coa_name."</td>"; } ?></td>
                        <td><?php echo $hasil3->jumlah1; ?></td>
                      </tr>
                     <?php } ?>
                    </tbody>
                    <thead class="table-header report-header">
                      <tr style="background-color: green !important">
                        <th class="border-bottom border-sides in-the-middle">
                        Total
                        </th>
                        <th class="text-center border-bottom border-sides">
                        </th>
                      </tr>
                    </thead>
                  </table>
                  <table style="width:100%" class="table table-hover table-striped dataTable order-list" id="myTable">
                    <thead class="table-header report-header">
                      <tr>
                        <th class="border-bottom border-sides in-the-middle" style="width: 50%">
                        Akun
                        </th>
                        <th class="text-center border-bottom border-sides">
                        Saldo Akhir
                        </th>
                        
                      </tr>
                    </thead>
                    <tbody>

                    <?php 
                      foreach($data4->result() as $hasil4) { 
                        ?>
                      <tr>
                        <?php if($hasil4->coa_id == "a") {echo "<td><b>".$hasil4->coa_category_name."</b></td>";} else {echo "<td style='padding-left:20px'>".$hasil4->coa_name."</td>"; } ?></td>
                        <td><?php echo $hasil4->jumlah1; ?></td>
                      </tr>
                     <?php } ?>
                    </tbody>
                    <thead class="table-header report-header">
                      <tr style="background-color: green !important">
                        <th class="border-bottom border-sides in-the-middle">
                        Total
                        </th>
                        <th class="text-center border-bottom border-sides">
                        </th>
                      </tr>
                    </thead>
                  </table>
                  <table style="width:100%" class="table table-hover table-striped dataTable order-list" id="myTable">
                    <thead class="table-header report-header">
                      <tr>
                        <th class="border-bottom border-sides in-the-middle" style="width: 50%">
                        Akun
                        </th>
                        <th class="text-center border-bottom border-sides">
                        Saldo Akhir
                        </th>
                        
                      </tr>
                    </thead>
                    <tbody>

                    <?php 
                      foreach($data5->result() as $hasil5) { 
                        ?>
                      <tr>
                        <?php if($hasil5->coa_id == "a") {echo "<td><b>".$hasil5->coa_category_name."</b></td>";} else {echo "<td style='padding-left:20px'>".$hasil5->coa_name."</td>"; } ?></td>
                        <td><?php echo $hasil5->jumlah1; ?></td>
                      </tr>
                     <?php } ?>
                    </tbody>
                    <thead class="table-header report-header">
                      <tr style="background-color: green !important">
                        <th class="border-bottom border-sides in-the-middle">
                        Total
                        </th>
                        <th class="text-center border-bottom border-sides">
                        </th>
                      </tr>
                    </thead>
                  </table>
                </div>
              </div>
              <?php  } ?>
              </div>
            </div>
            <!-- End Example Responsive -->
           </div>
        </div>
  </div>
</div>
<!-- End Page -->
 
  

 	


<script type="text/javascript">      
  $('#exampleTableTools').DataTable({
  "paging": true,
  "lengthChange": false,
  "searching": false,
  "ordering": false,
  "info": true,
  "autoWidth": true
});

  $('.datepicker').datepicker({
    orientation: 'bottom'
})

</script>

<script>
    toastr.options = {
      "closeButton": true,
      "debug": false,
      "newestOnTop": false,
      "progressBar": false,
      "positionClass": "toast-top-right",
      "preventDuplicates": false,
      "onclick": null,
      "showDuration": "300",
      "hideDuration": "500",
      "timeOut": "5000",
      "extendedTimeOut": "1000",
      "showEasing": "swing",
      "hideEasing": "linear",
      "showMethod": "fadeIn",
      "hideMethod": "fadeOut"
    }
</script>

<?php 
    if(isset($_GET['msg'])){
        if($_GET['msg']=="delsuccess"){ 
            echo  "<script>";
            echo   "toastr['success']('Data Berhasil dihapus');";
            echo  "</script>";
        } 
    }
?>