<div class="page">
  <div class="page-header">
    <h1 class="page-title">Ubah Data</h1>
    <div class="page-header-actions">
      <!-- <button type="button" class="btn btn-sm btn-icon btn-primary btn-round waves-effect waves-classic" data-toggle="tooltip" data-original-title="Edit">
        <i class="icon md-edit" aria-hidden="true"></i>
      </button>
      <button type="button" class="btn btn-sm btn-icon btn-primary btn-round waves-effect waves-classic" data-toggle="tooltip" data-original-title="Refresh">
        <i class="icon md-refresh-alt" aria-hidden="true"></i>
      </button>
      <button type="button" class="btn btn-sm btn-icon btn-primary btn-round waves-effect waves-classic" data-toggle="tooltip" data-original-title="Setting">
        <i class="icon md-settings" aria-hidden="true"></i>
      </button> -->
    </div>
  </div>
  <div class="page-content">
    <!-- Panel -->
    <div class="panel">
      <div class="panel-body">
        <div class="col-md-4">
          <form  method="post" id="thisform">
          <input type="hidden" name="flag" value="edit">
          <?php
              $hasil3 = $data3->result()[0];
          ?>
          <input type="hidden" name="idcoano" value="<?php echo $hasil3->id ?>">
          <div class="form-group">
              <h4 class="">Company</h4>
              <input type="text" class="form-control" name="company" autocapitalize="words" value="<?php echo $hasil3->company; ?>">
          </div>
        <div class="form-group">
              <h4 class="">Name</h4>
              <input type="text" class="form-control" name="nama" autocapitalize="words" value="<?php echo $hasil3->name; ?>">
          </div>
           <div class="form-group">
              <h4 class="">Job Title</h4>
              <input type="text" class="form-control" name="jobtitle" autocapitalize="words" value="<?php echo $hasil3->job_title; ?>">
          </div>
           <div class="form-group">
              <h4 class="">No Telp</h4>
              <input type="text" class="form-control" name="notlp" autocapitalize="words" value="<?php echo $hasil3->no_tlp; ?>">
          </div>
           <div class="form-group">
              <h4 class="">No Fax</h4>
              <input type="text" class="form-control" name="nofax" autocapitalize="words" value="<?php echo $hasil3->no_fax; ?>">
          </div>

          <div class="form-group">
              <h4 class="">Address</h4>
              <textarea class="form-control" name="address" rows="5"><?php echo ($hasil3->address) ?></textarea>
          </div>

            <div class="form-group">
              <h4 class="">Email</h4>
              <input type="text" class="form-control" name="email" autocapitalize="words" value="<?php echo $hasil3->email; ?>">
          </div>

            <div class="form-group">
              <h4 class="">Web</h4>
              <input type="text" class="form-control" name="web" autocapitalize="words" value="<?php echo $hasil3->web; ?>">
          </div>
                
        </div>
        <!-- End Example Responsive -->
      </div>
    </div>
    <div class="form-group text-right">
        <button type="button" class="btn btn-warning waves-effect waves-classic" onclick="window.history.back()">Cancel</button>
        <button type="submit" class="btn btn-primary waves-effect waves-classic" id="btnsubmit">Submit </button>
    </div>
    </form>
  </div>
</div>
<!-- End Page -->


