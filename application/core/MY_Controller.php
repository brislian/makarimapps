<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class MY_Controller extends CI_Controller{
  function render_page($content, $data = NULL){

        $data['header'] = $this->load->view('template/default/header', $data, TRUE);
        $data['content'] = $this->load->view($content, $data, TRUE);
        $data['scriptjs'] = $this->load->view('template/default/scriptjs', $data, TRUE);
        $data['footer'] = $this->load->view('template/default/footer', $data, TRUE);
        
        $this->load->view('template/default/index', $data);
    }

    function render_login($content, $data = NULL){

        $data['header'] = $this->load->view('template/header', $data, TRUE);
        $data['navigation'] = NULL;
        $data['content'] = $this->load->view($content, $data, TRUE);
        //$data['footer'] = $this->load->view('template/footer', $data, TRUE);
        
        $this->load->view('template/index', $data);
    }

    function render_pagenonadmin($content, $data = NULL){

        $data['header'] = $this->load->view('template/default/header', $data, TRUE);
        $data['content'] = $this->load->view($content, $data, TRUE);
        $data['scriptjs'] = $this->load->view('template/default/scriptjs', $data, TRUE);
        $data['footer'] = $this->load->view('template/default/footer', $data, TRUE);
        
        $this->load->view('template/default/index', $data);

        // $data['header'] = $this->load->view('template/header', $data, TRUE);
        // $data['topbar'] = $this->load->view('template/tobar', $data, TRUE);
        // $data['navigation'] = $this->load->view('template/navigation2', $data, TRUE);
        // $data['content'] = $this->load->view($content, $data, TRUE);
        // $data['footer'] = $this->load->view('template/footer', $data, TRUE);
        
        // $this->load->view('template/index', $data);
    }
}