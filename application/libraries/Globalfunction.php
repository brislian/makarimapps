<?php
Class GlobalFunction {
	public function getsuburusan($urusan){
      $CI =& get_instance();
      $CI->load->model('M_Global');
      $data = $CI->M_Global->getmultiparam("MS_SUBURUSAN","STATUS='1' and KODEDANABELANJA = '2' and NOKODEURUSAN = '$urusan'");
      return $data;
  	}
	public function gettahun(){
      $CI =& get_instance();
      $CI->load->model('M_Global');
      $data = $CI->M_Global->distinct("TAHUNBELANJA","MS_TAHUNBELANJA","STATUS = 1");
      return $data;
  	}

	public function getdinas($suburusan){
      $CI =& get_instance();
      $CI->load->model('M_Global');
      $data = $CI->M_Global->getmultiparam("MS_DINAS",$suburusan);
      return $data;
  	}

  	public function getprogram($dinas){
      $CI =& get_instance();
      $CI->load->model('M_Global');
      $data = $CI->M_Global->getmultiparam("MS_PROGRAM",$dinas);
      return $data;
  	}

  	public function getkegiatan($program){
      $CI =& get_instance();
      $CI->load->model('M_Global');
      $data = $CI->M_Global->getmultiparam("MS_KEGIATAN",$program);
      return $data;
  	}

  	public function getrealisasi($tahun,$bulan,$nokegiatan){
      $CI =& get_instance();
      $CI->load->model('M_Global');
      $data = $CI->M_Global->getmultiparam("MS_INPUTREALISASI","TAHUNBELANJA = '$tahun' and BULAN = '$bulan' and NOKEGIATAN = '$nokegiatan'");
      return $data;
  	}

  	public function getrealisasibtl($tahun,$bulan,$nodinas){
      $CI =& get_instance();
      $CI->load->model('M_Global');
      $data = $CI->M_Global->getmultiparam("MS_INPUTREALISASIBTL","TAHUNBELANJA = '$tahun' and BULAN = '$bulan' and NODINAS = '$nodinas'");
      return $data;
  	}

  	public function getjumlah($table, $kolom, $param) {
	  $CI =& get_instance();
      $CI->load->model('M_Global');
      $data = $CI->M_Global->getsum($table,$kolom,$param);
      foreach ($data->result() as $hasil) {
      	$abc = $hasil->JUMLAH;
      }
      return $abc;
	}

	public function getjumlah2($table, $kolom, $param) {
	  $CI =& get_instance();
      $CI->load->model('M_Global');
      $data = $CI->M_Global->getsum($table,$kolom,$param);
      foreach ($data->result() as $hasil) {
      	$abc = $hasil->JUMLAH;
      }
      return $this->konversirupiah2($abc);
	}

	public function konversirupiah($angka){
		$jadi = "IDR " . number_format($angka,2,',','.');
		return $jadi;
	}

	public function konversirupiah2($angka){
		$jadi = number_format($angka,0,',','.');
		return $jadi;
	}

	public function globalstatus($status){
		if($status == "0"){
			return "Non Aktif";
		} else if ($status == "1"){
			return "Aktif";
		}

	}
	public function warnatext(){
		$warnatext = array("text-custom","text-pink","text-warning","text-info"); 
		return $warnatext;
	}
	public function bulan(){
		$bulan = array("Januari","Februari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","Nopember","Desember");    
		return $bulan;
	}



	public function get_rnd_iv($iv_len)
	{
	$iv = '';
	while ($iv_len-- > 0) {
	$iv .= chr(mt_rand() & 0xff);
	}
	return $iv;
	}


	public function base64_encrypt($plain_text, $password, $iv_len = 16)
	{
	$plain_text .= "\x13";
	$n = strlen($plain_text);
	if ($n % 16) $plain_text .= str_repeat("\0", 16 - ($n % 16));
	$i = 0;
	$enc_text = $this->get_rnd_iv($iv_len);
	$iv = substr($password ^ $enc_text, 0, 512);
	while ($i < $n) {
	$block = substr($plain_text, $i, 16) ^ pack('H*', md5($iv));
	$enc_text .= $block;
	$iv = substr($block . $iv, 0, 512) ^ $password;
	$i += 16;
	}
	$hasil=base64_encode($enc_text);
	return str_replace('+', '@', $hasil);
	}
	 
	// fungsi base64 decrypt
	// untuk mendekripsi string base64
	public function base64_decrypt($enc_text, $password, $iv_len = 16)
	{
	$enc_text = str_replace('@', '+', $enc_text);
	$enc_text = base64_decode($enc_text);
	$n = strlen($enc_text);
	$i = $iv_len;
	$plain_text = '';
	$iv = substr($password ^ substr($enc_text, 0, $iv_len), 0, 512);
	while ($i < $n) {
	$block = substr($enc_text, $i, 16);
	$plain_text .= $block ^ pack('H*', md5($iv));
	$iv = substr($block . $iv, 0, 512) ^ $password;
	$i += 16;
	}
	return preg_replace('/\\x13\\x00*$/', '', $plain_text);
	}

	public function extract_var($data){
	$raw = explode('&',$data);
	     for ($i=0; $i <= count($raw)-1; $i++)
	      {
	         $data = explode('=', $raw[$i]);
	         $var[$data[0]] = $data[1];  
	      }
	      return $var;
	}

	public function key(){
		return "sdfjk34394sfd";
	}

}
?>